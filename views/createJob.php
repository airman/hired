<?php

include '../config.php';
include_once MODEL_DIR . 'Job.php';
include_once MODEL_DIR . 'Skill.php';
include_once HELPER_DIR . 'filters.php';
include_once HELPER_DIR . 'utils.php';
include_once HELPER_DIR . "model_creators.php";

$keys = array(
    'formToken',
    'title',
    'description',
    'fieldID',
    'skills',
    'cityName'
);

if(arrayKeysSet($keys, $_POST)){
    if(isFormTokenValid($_POST['formToken'])){
        $pdo = newPDO();
        $fieldID = $_POST['fieldID'];
        $cityID = createCity($_POST['cityName'], $pdo);
        $_POST['creatorID'] = $_SESSION['user']['id'];
        $_POST['cityID'] = $cityID;
        

        $job = Job::create($_POST, $pdo);
        if ($job) {
            $skillNames = explode(',', $_POST['skills']);
            for($i =0; $i < count($skillNames); $i++){
                $skillNames[$i] = trim($skillNames[i]);
            }
            createSkills($fieldID, $skillNames, $pdo);

            Skill::setJobSkills($job->getJobID(), $skillNames, $pdo);
            setAlert(
                "Job successfully created.",
                $ALERT_SUCCESS
            );
        } else {
            throw new Xception(
                "Job creation failed",
                Xception::$ERR_UNEXPECTED,
                array_merge($keys, $_POST)
            );
        }
    }else{
        throw new Xception(
            "Invalid form token",
            Xception::$ERR_INVALID_TOKEN,
             $_POST['formToken']
        );
    }
}else{

    throw new Xception(
        "Missing request parameters for job creation",
        Xception::$ERR_MISSING_DATA,
        array_merge($keys, $_POST)
    );
}

header("Location: " . HOME_URL . "?page=home");
