<?php
	$view->addCSS('style.css');
	$view->addCSS('navbar.css');
	$view->addCSS('controls.css');
	$view->addCSS('job_list.css');
	$view->addCSS('upcoming.css');
    $view->addJS('validation.js');
    $view->setAttribute('title', 'Home');

    require_once MODEL_DIR . "Job.php";
    require_once MODEL_DIR . "City.php";
    require_once MODEL_DIR . "Field.php";
    require_once HELPER_DIR . "time_utils.php";

    include SNIPPET_DIR . 'head.php';

    $date = currentDate();

    $resultPage = 1;
    $resultSize = 10;

    $currentJobs = array_merge(
        Job::getAllByWorkerIDStatus($userID, JOB::$JOB_PENDING, $pdo),
        Job::getAllByCreatorIDStatus($userID, JOB::$JOB_PENDING, $pdo),
        Job::getAllByCreatorIDStatus($userID, JOB::$JOB_AVAILABLE, $pdo)
    );
    $appliedJobs = Job::getAllByUserIDApplicationState($userID, JOB::$JOB_APP_PENDING, $pdo);
    $currentJobCount = count($currentJobs);
    $appliedJobCount = count($appliedJobs);
?>
<body>
<div id="overlay" class="hidden"></div>	
<?php include SNIPPET_DIR . 'navbar.php'; ?>
<div id="wrapper" class="container">
    <?php echo getAlertHTML() . PHP_EOL; ?>
    <div class="row">
        <!-- <div class="col-md-3 left-column column hidden-xs"></div> -->
        <div class="col-md-7 mid-column column">
            <div class="tab-list">
                <ul>
                    <li class="tab-list-item active" id="btnAvailableJobs">Available</li>
                    <li class="tab-list-item" id="btnCurrentJobs">Current <span class="badge hidden-xs"><?php echo $currentJobCount;?></span></li>
                    <li class="tab-list-item" id="btnAppliedJobs">Applied <span class="badge hidden-xs"><?php echo $appliedJobCount;?></span></li> <li class="tab-list-item hidden-xs" id="btnCreateJob"><span class="fa-plus"></span> New Job</li>
                </ul>
            </div>
            <div id="tabCreateJob" class="tab-view">
                <form id="formCreateJob" action="?page=createJob" method="POST">
                    <legend>Create New Job</legend>
                    <input type="hidden" name="formToken" value="<?php echo getFormToken(); ?>">
                    <div class="row">
                        <div class="col-md-4">
                            <select class="form-control" name="fieldID">
                                <?php
                                    $fields = Field::getAll($pdo);
                                    foreach($fields as $field){
                                        echo '<option value="' . $field->getFieldID() . '">' . 
                                            $field->getName() . "</option>" . PHP_EOL;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="Job title" name="title">
                        </div>
                    </div><br/>
                    <div class="row">
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="cityName" placeholder="Job City">
                        </div>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="skills" placeholder="Skills required separated by comma">
                        </div>
                    </div><br/>
                    <textarea name="description" id="" cols="30" rows="5" class="form-control" placeholder="Job description"></textarea><br/>
                    <button class="btn btn-success">Create</button>
                    <a href="#" class="btn btn-default" onclick="showAvailableJobs()">Cancel</a>
                </form>
            </div>
            <div class="tab-view" id="tabCurrentJobs">
                <div class="feed-item job feed-header">
                    <div class="job-details">
                        <span class="job-title">Working Jobs</span>
                    </div>
                </div>
                <?php
                    $start = 0;
                    $jobs = Job::getAllByWorkerIDStatus($userID, JOB::$JOB_PENDING, $pdo);
                    include SNIPPET_DIR . "job_list.php";
                ?>
                <div class="feed-item job feed-header">
                    <div class="job-details">
                        <span class="job-title">Created Jobs</span>
                    </div>
                </div>
                <?php
                    $start = 0;
                    $jobs = array_merge(
                        Job::getAllByCreatorIDStatus($userID, JOB::$JOB_PENDING, $pdo),
                        Job::getAllByCreatorIDStatus($userID, JOB::$JOB_AVAILABLE, $pdo)
                    );
                    include SNIPPET_DIR . "job_list.php";
                    include SNIPPET_DIR . "result_info.php";
                ?>
            </div>
            <div class="tab-view" id="tabAppliedJobs">
                <?php
                    $start = 0;
                    $jobs = $appliedJobs;
                    include SNIPPET_DIR . "job_list.php";
                    include SNIPPET_DIR . "result_info.php";
                ?>
            </div>
            <div id="tabAvailableJobs" class="tab-view">
                <?php
                    $start = 0;
                    $jobs = Job::getAllAvailable($pdo);
                    include SNIPPET_DIR . "job_list.php";
                    if($jobCount < 1){
                ?>
                    <div class="feed-item job">
                        <div class="job-details">
                            <br/>
                            <span class="job-title"></span>
                            <br/>
                        </div>
                    </div>

                <?php
                    }
                    include SNIPPET_DIR . "result_info.php";
                ?>
            </div>
        </div>
        <div class="col-md-5 right-column column hidden-xs">
            <div class="upcoming">
                <div class="title-bar">Upcoming</div>
                <?php include SNIPPET_DIR . "upcoming_users.php"; ?>
            </div>
        </div>
<script>
    function autocollapse() {
        var navbar = $('#autocollapse');
        navbar.removeClass('collapsed'); // set standard view
        if(navbar.innerHeight() > 60){ // check if we've got 2 lines
            navbar.addClass('collapsed'); // force collapse mode
            if($("#btnCreateJob").prop('class').contains('active')){
                showAvailableJobs();
            }
        }
    }

    function deactivateTabs(){
        $(".tab-list-item").removeClass('active');
    }

    function hideTabViews(){
        $(".tab-view").hide();
    }
    function showCreateJob(){
        deactivateTabs();
        hideTabViews();
        $("#tabCreateJob").show();
        $("#btnCreateJob").addClass('active');
    }

    function showAvailableJobs(){
        deactivateTabs();
        hideTabViews();
        $("#tabAvailableJobs").show();
        $("#btnAvailableJobs").addClass('active');
    }

    function showCurrentJobs(){
        deactivateTabs();
        hideTabViews();
        $("#tabCurrentJobs").show();
        $("#btnCurrentJobs").addClass('active');
    }

    function showAppliedJobs(){
        deactivateTabs();
        hideTabViews();
        $("#tabAppliedJobs").show();
        $("#btnAppliedJobs").addClass('active');
    }

    function expandJob(jobID){
        $("#job-" + jobID + " .job-description").show();
    }

    function collapseJob(jobID){
        $("#job-" + jobID + " .job-description").hide();
    }

    function applyForJob(jobID){
        form = document.forms["formApplyForJob"];
        form["jobID"] = jobID;
        form.submit();
    }

    function collapseAllJobs(){
        $(".job-description").hide();
    }

    function toggleJob(jobID){
        $("#job-" + jobID + " .job-description").toggle();
        $("#job-" + jobID + " .toggleDescription").toggleClass('active');
    }

    $().ready(function(){
        autocollapse();
        $(window).on('resize', autocollapse);
        $("#btnCreateJob").click(showCreateJob);
        $("#btnAvailableJobs").click(showAvailableJobs);
        $("#btnCurrentJobs").click(showCurrentJobs);
        $("#btnAppliedJobs").click(showAppliedJobs);
        showAvailableJobs();
        collapseAllJobs();
        initAlerts();
    });
</script>
</body>
</html>
