<?php
    include '../config.php';
    include_once MODEL_DIR . 'Job.php';
    include_once HELPER_DIR . 'filters.php';
    include_once HELPER_DIR . 'utils.php';
    include_once HELPER_DIR . "model_creators.php";

    $keys = array(
        'formToken',
        'jobID'
    );

    if(arrayKeysSet($keys, $_POST)){
        if(isFormTokenValid($_POST['formToken'])){
            $pdo = $user->getPdo();
            $job = Job::getByJobID($_POST['jobID'], $pdo);
            if($job != null){
                $job->apply($user->getUserID());
                setAlert(
                   "Application sent.",
                    $ALERT_SUCCESS
                );
                header("Location: " . HOME_URL . "?page=home");
            }else {
                throw new Xception(
                    "Job application failed",
                    Xception::$ERR_UNEXPECTED,
                    array_merge($keys, $_POST)
                );
            }
        }else{
            throw new Xception(
                "Invalid form token",
                Xception::$ERR_UNEXPECTED,
                array_merge($keys, $_POST)
            );
        }
    }else{
        throw new Xception(
            "Missing request parameters for job application",
            Xception::$ERR_MISSING_DATA,
            array_merge($keys, $_POST)
        );
    }
