<?php
    
    include '../config.php';
    include_once CLASS_DIR . 'Xception.php';
    include_once MODEL_DIR . 'User.php';
    include_once MODEL_DIR . 'Profile.php';
    include_once HELPER_DIR . 'filters.php';
    include_once HELPER_DIR . 'utils.php';


    $keys = array(
        'formToken',
        'email',
        'password',
        'password2',
        'firstName',
        'lastName',
        'gender'
    );
    if(!isLoggedIn()){
        if(arrayKeysSet($keys, $_POST)){
            if(isFormTokenValid($_POST['formToken'])){
                if($_POST['password2'] != $_POST['password']){
                    throw Xception(
                        'Passwords do not match',
                        Xception::ERR_INVALID_DATA,
                        $_POST
                    );
                }
                $pdo = newPDO();
                $_POST['email'] = strtolower($_POST['email']);
                $rcode = rand(0,9) . rand(0,9) . rand(0,9);
                $username = $_POST['firstName'] . '.' . $_POST['lastName'] . $rcode;
                $_POST['username'] = $username;
                $user = User::create($_POST, $pdo);
                if($user == null){
                    setAlert(
                        "Email already in use.",
                        $ALERT_DANGER
                    );

                }
                $_POST['userID'] = $user->getUserID();
                $profile = Profile::create($_POST, $pdo);
                login(
                    $user, 
                    HOME_URL . '?page=home', 
                    HOME_URL . '?page=signup' 
                );
            }else{
                # TODO log error
                setAlert(
                    "Looks like somebody clicked on a stale link, refresh and try again.",
                    $ALERT_WARNING
                );
            }
        }else{
            # TODO log error
            setAlert(
                "I don't think you're supposed to do that, your request has been logged.",
                $ALERT_WARNING
            );
        }
    }
head('Location: ' . HOME_URL);
