<?php
$view->addCSS('style.css');
$view->addCSS('navbar.css');
$view->addCSS('controls.css');
$view->addJS('validation.js');
$view->setAttribute('title', 'Connections');
include SNIPPET_DIR . 'head.php';
?>
<body>
<div id="overlay" class="hidden"></div>
<?php include SNIPPET_DIR . 'navbar.php'; ?>
<div id="container" class="container-fluid">
    <?php echo getAlertHTML(); ?>
    <div class="row">
        <!-- Left Column -->
        <div class="col-md-3 column left-column">
            <?php include SNIPPET_DIR . 'user_group.php';?>
        </div>

        <!-- Mid Column -->
        <div class="col-md-6 column mid-column">
            <!-- Main Feed -->
            <div class="main-feed rounded">
                <!-- Search Options -->
                <div class="search-options top-rounded">
                    <div class="btn-group sort-actions" data-toggle="buttons">
                        <label class="btn btn-default sort-action">
                            <input type="radio"/>
                            <img src="assets/img/icons/sort-ascending.svg" alt="" class="sort-icon">
                        </label>
                        <label class="btn btn-default sort-action">
                            <input type="radio" />
                            <img src="assets/img/icons/sort-descending.svg" alt="" class="sort-icon">
                        </label>
                    </div>
                </div>
                <?php include SNIPPET_DIR . 'notifications_feed.php';?>
            </div>
        </div>

        <!-- Right Column -->
        <div class="col-md-3 column right-column hidden-sm">
            <!--            <div class="upcoming">-->
            <!--                <div class="row">-->
            <!---->
            <!--                </div>-->
            <!--            </div>-->
        </div>
    </div>
</div>
<input type="hidden" name="formToken" id="formToken" value="<?php echo getFormToken(); ?>">
<?php include SNIPPET_DIR . 'edit_profile_dialog.php' ?>
<?php include SNIPPET_DIR . 'new_job_dialog.php' ?>
<script>

    function showNewJob(){
        $("#new-job-dialog").removeClass('hidden');
        $("#overlay").removeClass('hidden');
    }

    function hideNewJob(){
        $("#new-job-dialog").addClass('hidden');
        $("#overlay").addClass('hidden');
    }
    $().ready(function(){
        // $('.btn').button();
        initAlerts();
        initProfile();
        $("#btnNewJob").click(showNewJob);
        $("#btnCancelJob").click(hideNewJob);
    });
</script>
</body>
</html>
