<?php
	$view->addCSS('style.css');
	$view->addCSS('navbar.css');
	$view->addCSS('controls.css');
    $view->setAttribute('title', 'Profile');
	include SNIPPET_DIR . 'head.php';

    //if(isset($_GET['u']
?>
<body>
<?php include SNIPPET_DIR . 'navbar.php';?>
<div id="wrapper" class="container">
    <?php echo getAlertHTML() . PHP_EOL; ?>
    <div class="row">
        <!-- <div class="col-md-3 left-column column hidden-xs"></div> -->
        <div class="col-md-7 mid-column column">
            <div class="tab-list">
                <ul>
                    <li class="tab-list-item active" id="btnAvailableJobs">Available</li>
                    <li class="tab-list-item" id="btnCurrentJobs">Current <span class="badge hidden-xs"><?php echo $currentJobCount;?></span></li>
                    <li class="tab-list-item" id="btnAppliedJobs">Applied <span class="badge hidden-xs"><?php echo $appliedJobCount;?></span></li>
                    <li class="tab-list-item hidden-xs" id="btnCreateJob"><span class="fa-plus"></span> New Job</li>
                </ul>
            </div>
         </div>
        <div class="col-md-5 mid-column column">
        </div>
    </div>
</div>
<script>
    $().ready(function(){
        autocollapse();
        $(window).on('resize', autocollapse);
        initAlerts();
    });
</script>
</body>
</html>
