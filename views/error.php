<?php
$view->addCSS('error.css');
include SNIPPET_DIR . 'head.php';
?>
<body>
    <div id="wrapper" class="container">
        <img src="assets/img/error.jpg" alt="error"/>
        <div class="error-msg">
            <span id="code">
                ERROR <?php echo $_SESSION['error_code'];?>
            </span>
            <span id="msg">
                <?php echo $_SESSION['error_msg']; ?>
            </span>
        </div>
    </div>
</body>
</html>
