<?php
    require_once MODEL_DIR . 'User.php';
    require_once MODEL_DIR . 'Profile.php';
    require_once HELPER_DIR . 'filters.php';
    require_once HELPER_DIR . 'utils.php';

    $keys = array(
        'formToken',
        'email',
        'password',
    );

    if(arrayKeysSet($keys, $_POST)){
        if(isFormTokenValid($_POST['formToken'])){
            $pdo = newPDO();
            $email = strtolower($_POST['email']);
            $pass = $_POST['password'];
            $user = User::getByEmailPass($email, $pass, $pdo);
            login(
                $user, 
                '?page=home', 
                '?page=signup' 
            );

        }else{
            throw new Xception(
                "Invalid form token",
                Xception::$ERR_INVALID_TOKEN,
                $_POST['formToken']
            );
        }
    }else{
        throw new Xception(
            "Missing request parameters",
            Xception::$ERR_MISSING_DATA,
            array_merge($keys, $_POST)
        );
    }
