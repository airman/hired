<?php
    include '../config.php';
    include_once MODEL_DIR . 'Location.php';
    include_once MODEL_DIR . 'Profile.php';
    include_once HELPER_DIR . 'validation.php';
    include_once HELPER_DIR . 'filters.php';
    include_once HELPER_DIR . 'model_creators.php';

    $keys = array(
        'phones', 'locations',
        'emails', 'formToken',
        'firstName', 'lastName',
        'bio'
    );

    if(!arrayKeysSet($keys, $_POST)) {
        var_dump(array_keys($_POST));
        echo "<br/>";
        var_dump($keys);
        die("Incomplete request");
    }

    $emails = explode(',', str_replace(' ', '', $_POST['emails']));

    $profile->setFirstName($_POST['firstName']);
    $profile->setLastName($_POST['lastName']);
    $profile->setBio($_POST['bio']);
    $profile->save();
echo "p0<br>";
    $pdo = $user->getPdo();
    $stmnt = $pdo->prepare("DELETE FROM contacts WHERE userID = :userID");
    $stmnt->bindValue(":userID", $user->getUserID(), PDO::PARAM_INT);
    $stmnt->execute();

    $stmnt = $pdo->prepare("DELETE FROM user_locations WHERE userID = :userID");
    $stmnt->bindValue(":userID", $user->getUserID(), PDO::PARAM_INT);
    $stmnt->execute();

echo "p1<br>";
    // insert phone numbers
    $contactType = 'P';
    $stmnt = $pdo->prepare(
        sprintf("INSERT INTO contacts (%s) VALUES (%s)",
                "userID, contactInfo, contactType",
                ":userID, :contactInfo, :contactType"
            )
    );
    $stmnt->bindParam(":userID", $user->getUserID(), PDO::PARAM_INT);
    $stmnt->bindParam(":contactType", $contactType, PDO::PARAM_STR);
    foreach(explode(',', str_replace(' ', '', $_POST['phones'])) as $phone){
        if(isPhoneNumberValid($phone)){
            $stmnt->bindValue(":contactInfo", $phone, PDO::PARAM_STR);
            $stmnt->execute();
        }else{
            echo "$phone<br/>";
        }
    }

echo "p2<br>";
    // insert emails
    $contactType = 'E';
    foreach(explode(',', str_replace(' ', '', $_POST['emails'])) as $email){
        if(isEmailValid($email)){
            $stmnt->bindValue(":contactInfo", $email, PDO::PARAM_STR);
            $stmnt->execute();
        }
    }


// insert locations
echo "p3<br>";

createLocations(explode(',', str_replace(' ', '', $_POST['locations'])), $pdo);
$stmnt = $pdo->prepare(
    sprintf("INSERT INTO user_locations (%s) VALUES (%s)",
        "userID, locationID",
        ":userID, :locationID"
    )
);
$stmnt->bindParam(":userID", $user->getUserID(), PDO::PARAM_INT);
foreach(explode(',', str_replace(' ', '', $_POST['locations'])) as $locationName){
    $loc = Location::getByName($locationName, $pdo);
    if($loc){
        $stmnt->bindValue(":locationID", $loc->getLocationID(), PDO::PARAM_INT);
        $stmnt->execute();
    }
}
setAlert("Profile updated.", $ALERT_SUCCESS);
header("Location: " . HOME_URL);
