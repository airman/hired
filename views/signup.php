<?php
	$view->addCSS('signup.css');
	$view->addCSS('controls.css');
	$view->addJS('validation.js');
	include SNIPPET_DIR . 'head.php';
?>
<body>
	<div id="wrapper" class="container-fluid">
        <?php echo getAlertHTML();?>
        <div class="row">
            <div id="board" class="col-md-9 col-sm-12 column">
                <span class="board-title">Part Timer</span>
                <ul>
                    <li>Make extra cash by doing jobs in your city</li>
                    <li>while building a great community profile</li>
                </ul>
            </div>
            <div id="sidebar" class="col-md-3 column">
                <form id="formLogin" class="form" action="?page=login" method="POST">
                    <legend>Login</legend><br/>
                    <input type="hidden" name="formToken" value="<?php echo getFormToken(); ?>">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-user"></span>
                        </span>
                        <input class="form-control" id="email" type="email" name="email" placeholder="Email" required>
                    </div><br/>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-lock"></span>
                        </span>
                        <input class="form-control" type="password" name="password" placeholder="Password" required>
                    </div><br/>
                    <button class="btn btn-info">Log In</button>
                </form><br/>
                <form id="formSignUp" class="form" action="/hired/?page=register" method="POST">
                    <legend>Sign Up</legend>
                    <input type="hidden" name="formToken" value="<?php echo getFormToken(); ?>">
                    <span id="first-name-helper" class="helper text-danger ">Confirmation helper</span><br/>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-user"></span>
                        </span>
                        <input class="form-control editbox" type="text" name="firstName" placeholder="First Name" required
                            pattern="^[a-zA-Z\-]+$"
                        >
                    </div>
                    <span id="last-name-helper" class="helper text-danger ">Confirmation helper</span><br/>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-user"></span>
                        </span>
                        <input class="form-control editbox" type="text" name="lastName" placeholder="Last Name" required
                            pattern="^[a-zA-Z\-]+$"
                        >
                    </div>
                    <span id="email-helper" class="helper text-danger ">*Enter your email</span><br/>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-user"></span>
                        </span>
                        <input id="email" class="form-control editbox warning"type="email" name="email" placeholder="Email"
                    pattern="^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$" required
                        >
                    </div>
                    <span id="password-helper" class="helper text-danger ">Password helper</span><br/>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-lock"></span>
                        </span>
                        <input id="pword1" class="form-control editbox" type="password" name="password" placeholder="New Password" required>
                    </div>
                    <span id="password-helper2" class="helper text-danger ">Confirmation helper</span><br/>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="fa-shield"></span>
                        </span>
                        <input id="pword2"class="form-control editbox" type="password" name="password2" placeholder="Password Confirmation" required>
                    </div><br/>
                    <div class="btn-group radio-group gender" data-toggle="buttons">
                        <label class="btn btn-info radio-button">
                            <input class="rbGender" type="radio" id="rbMale" name="gender" value="M" required checked/>Male
                        </label>
                        <label class="btn btn-info radio-button">
                            <input class="rbGender" type="radio" id="rbFemale" name="gender" value="F" required/>Female
                        </label>
                    </div><br/>
                    <button class="btn btn-success">Sign Up</button>
                </form>
        </div>
	</div>
	<script>
		emailHelper = $("#email-helper");
		pwordHelper1 = $("#password-helper");
		pwordHelper2 = $("#password-helper2");
		firstNameHelper = $("#first-name-helper");
		lastNameHelper = $("#last-name-helper");
		helpers = $(".helper");

		function setHelperToDanger(helper){
			helper.prop("class", "helper text-danger");		
		}
		
		function setHelperToWarning(helper){
			helper.prop("class", "helper text-warning");
		}
		
		function setHelperToInfo(helper){
			helper.prop("class", "helper text-info");
		}
		
		function checkPasswordMatch(){
			pword1 = $("#pword1").prop("value");
			pword2 = $("#pword2").prop("value");
			setHelperToDanger(pwordHelper2);
			if(pword1 == pword2){
				pwordHelper2.addClass("hidden");
				return true;
			}
			pwordHelper2.html("Passwords do not match.");
			pwordHelper2.removeClass("hidden");
			return false;
		}
		
		
		function validateEmail(){
            email = $("#email").prop("value");
			setHelperToDanger(emailHelper);
            if(email == ""){
        		emailHelper.html("Please enter your email address.");
        		emailHelper.removeClass("hidden");
            	return false;
            }
            if(isEmailValid(email)){
            	if(emailExists()){
            		emailHelper.html("That email address is already in use.");
            		emailHelper.removeClass("hidden");
            	}
            	emailHelper.addClass("hidden");
            	return true;
            }

            emailHelper.html("Please enter a <u>valid</u> email address.");
            emailHelper.removeClass("hidden");		
            return false;
		}

		function validatePasswords(){
			var valid = checkPasswordMatch();
			pword1 = $("#pword1").prop("value");
			pword2 = $("#pword2").prop("value");
			if(pword1 == ""){
                setHelperToInfo(pwordHelper1);
				pwordHelper1.html("Please enter your new password.");
				valid = false;
			} else if(pword1.length < 6){
				setHelperToInfo(pwordHelper1);
				pwordHelper1.html("Must be 6 characters or more.");
				valid = false;
			}else{
				pwordHelper1.addClass('hidden');
			}
			if(pword2 == ""){
                setHelperToInfo(pwordHelper2);
				pwordHelper2.html("Please confirm your new password.");
				pwordHelper2.removeClass("hidden");
				valid = false;
            }else{
				pwordHelper2.addClass("hidden");
            }
			return valid;
		}

		function validateAccountDetails(){
			emailValid = validateEmail();
			pwordValid = validatePasswords();
			var valid = emailValid && pwordValid;
			if(valid){
				emailHelper.html('Email');
				pwordHelper1.html('Password');
				pwordHelper2.html('Password Confirmation');
				firstNameHelper.html('First Name');
				lastNameHelper.html('Last Name');
				helpers.removeClass('hidden');
			}
			return valid;
		}


		$().ready(function(){
			$("#pword1").keyup(validatePasswords);
			$("#pword2").keyup(checkPasswordMatch);
			$("#rbMale").click();
            initAlerts();
            helpers.html("");
		});
	</script>

</body>
</html>
