<?php

require_once('config.php');
require_once('classes/ModelTest.php');

function newTestPDO(){
    $dsn = DSN;
    $dbhost = DB_HOST;
    $dbname = "hired_test";
    $dbuser = DB_USER;
    $dbpass = DB_PASS;

    $pdo = new PDO("$dsn:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
}

