
CREATE DATABASE IF NOT EXISTS hired_test;
USE hired_test;

DELETE FROM users;
  -- create user dinesh.naidoo
  -- create user mark.griffin
  -- create user oreo.biscuit
INSERT INTO users (userID, username, email, password) VALUES
    (1, 'dinesh.naidoo', 'dinesh@test.com', MD5('password')),
    (2, 'mark.griffin', 'mark@test.com', MD5('password')),
    (3, 'oreo.biscuits', 'oreo@test.com', MD5('password'))
;

DELETE FROM profiles;
  -- create profile for user dinesh.naidoo
  -- create profile for user mark.griffin
  -- create profile for user oreo.biscuit
INSERT INTO profiles (userID, firstName, lastName, gender, avatar, bio) VALUES
    (1, 'Dinesh', 'Naidoo', 'M', 'avatar-default.svg', 'The bio of Dinesh Naidoo.'),
    (2, 'Mark', 'Griffin', 'M', 'avatar-default.svg', 'The bio of Mark Griffin.'),
    (3, 'Oreo', 'Biscuits', 'F', 'avatar-default.svg', 'The bio of Oreo.')
;

DELETE FROM fields;
  -- create fieldA
  -- create fieldB
  -- create fieldC
INSERT INTO fields (fieldID, name, description) VALUES
    (4, 'FieldA', 'DescriptionA'),
    (5, 'FieldB', 'DescriptionB'),
    (6, 'FieldC', 'DescriptionC')
;

DELETE FROM skills;
  -- create skillA in fieldA
  -- create skillB in fieldB
  -- create skillC in fieldC
INSERT INTO skills (skillID, name, fieldID) VALUES
    (7, 'SkillA', 4),
    (8, 'SkillB', 5),
    (9, 'SkillC', 6)
;

DELETE FROM user_skills;
  -- dinesh skillA
  -- dinesh skillB
  -- mark skillC
INSERT INTO user_skills (userID, skillID) VALUES
    (1, 7),
    (1, 8),
    (2, 9)
;

DELETE FROM provinces;
  -- create provinceA
INSERT INTO provinces (provinceID, name) VALUES
    (10, 'ProvinceA')
;

DELETE FROM cities;
  -- create cityA in provinceA
  -- create cityB in provinceA
  -- create cityC in provinceA
INSERT INTO cities (cityID, name, provinceID) VALUES
    (11, 'CityA', 10),
    (12, 'CityB', 10),
    (13, 'CityC', 10)
;

DELETE FROM jobs;
  -- dinesh creates JobA
  -- dinesh creates JobB and hires mark
  -- dinesh creates JobC, hires mark, and mark completes job
  -- mark creates JobD
INSERT INTO jobs (jobID, creatorID, workerID, fieldID, cityID, title, description, status, points) VALUES
    (14, 1, NULL, 4, 11, 'JobA', 'JobA Description', 'A', 5),
    (15, 1, 2, 5, 12, 'JobB', 'JobB Description', 'P', 5),
    (16, 1, 2, 4, 13, 'JobC', 'JobC Description', 'C', 5),
    (17, 2, NULL, 6, 11, 'JobD', 'JobD Description', 'A', 5);

DELETE FROM job_skills;
  --  JobA requires skillA
  --  JobA requires SkillB
  --  JobB requires SkillB
  --  JobC requires SkillC
  --  JobD requires SkillA
INSERT INTO job_skills (jobID, skillID) VALUES
    (14, 7),
    (14, 8),
    (15, 8),
    (16, 9),
    (17, 7)
;

DELETE FROM job_applications;
  -- mark applies for JobB and is accepted
  -- mark applies for JobC and is accepted
  -- dinesh applies for JobD
INSERT INTO job_applications (jobID, userID, state) VALUES
    (15, 2, 'H'),
    (16, 2, 'H'),
    (17, 1, 'P')
;

DELETE FROM user_notifications;
  -- dinesh receives mark's application for JobB
  -- dinesh receives mark's application for JobC
  -- mark accepted for JobB
  -- mark accepted for JobC
  -- mark completes JobC
  -- mark receives dinesh's application for JobD
INSERT INTO user_notifications (notificationID, userID, message, unread) VALUES
    (18, 1, 'Mark applied for JobB', false),
    (19, 1, 'Mark applied for JobC', false),
    (20, 2, 'Accepted for JobB', false),
    (21, 2, 'Accepted for JobC', false),
    (22, 2, 'You have completed JobC', false),
    (23, 2, 'Dinesh applied for JobD', true)
;

DELETE FROM user_contacts;
  -- dinesh contact email
  -- dinesh contact phone number
  -- mark contact email
INSERT INTO user_contacts (userID, contactType, contactInfo) VALUES
    (1, 'E', 'danish.dinesh@test.com'),
    (1, 'P', '0123456789'),
    (2, 'E', 'marky.mark@test.com')
;

DELETE FROM user_locations;
  -- dinesh cityA
  -- dinesh CityB
  -- mark CityA
  -- mark CityB
INSERT INTO user_locations (userID, cityID) VALUES
    (1, 11),
    (1, 12),
    (2, 11),
    (2, 12)
;
