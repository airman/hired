<?php

require_once MODEL_DIR . 'Job.php';

class JobTest extends ModelTest{
    public $modelName = 'Job';
    protected $job = null;

    public function getJob(){
        if(!$this->job){
            $id = 15;
            $pdo = $this->getPdo();
            $this->job = Job::getByJobID($id, $pdo);
        }
        return $this->job;
    }

    public function validModelProvider()
    {
        return array(
            array(
                array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array(
                'Invalid creatorID' => array(
                    'creatorID' => '!1',
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                ),
                'Invalid title' => array(
                    'creatorID' => '1',
                    'title' => '#!@Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                ),
                'Invalid fieldID' => array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'fieldID' =>'!10',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                ),
                'Invalid cityID' => array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '!$18',
                    'description' => 'Test Job Description'
                ),
                'Invalid description' => array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '18',
                    'description' => 'Invalid'
                )
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing creatorID' => array(
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                ),
                'Missing title' => array(
                    'creatorID' => '1',
                    'fieldID' =>'10',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                ),
                'Missing fieldID' => array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'cityID'  => '18',
                    'description' => 'Test Job Description'
                ),
                'Missing cityID' => array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'description' => 'Test Job Description'
                ),
                'Missing description' => array(
                    'creatorID' => '1',
                    'title' => 'Test Job',
                    'fieldID' =>'10',
                    'cityID'  => '18'
                ),
            )
        );
    }

    /**
     * Test duplicate data entry
     * If successful, no Model should be created
     * @param $record = array record to be used to create model
     *
     * @dataProvider validModelProvider
     */
    public function testDuplicateModelCreation($record)
    {
    }

    ///@override
    public function testModelSave()
    {
        $pdo = $this->getPdo();
        $job = $this->getJob();

        //--------------------------------------------
        // Test Getters
        //--------------------------------------------
        $this->assertEquals($job->getTitle(), 'JobB');

        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------
        $newTitle = "BROOM";
        $newFieldID = 8;
        $newWorkerID = 8;
        $newCityID = 8;
        $newDescription = "New Job Description";
        $newStatus = 'C';
        $newPoints = 8;
        $job->setTitle($newTitle);
        $job->setFieldID($newFieldID);
        $job->setWorkerID($newWorkerID);
        $job->setCityID($newCityID);
        $job->setDescription($newDescription);
        $job->setStatus($newStatus);
        $job->setPoints($newPoints);
        $job->save();
        $id = $job->getJobID();
        $updatedJob = Job::getByJobID($id, $pdo);
        $this->assertEquals($updatedJob->getTitle(), $newTitle);
        $this->assertEquals($updatedJob->getFieldID(), $newFieldID);
        $this->assertEquals($updatedJob->getWorkerID(), $newWorkerID);
        $this->assertEquals($updatedJob->getCityID(), $newCityID);
        $this->assertEquals($updatedJob->getDescription(), $newDescription);
        $this->assertEquals($updatedJob->getStatus(), $newStatus);
        $this->assertEquals($updatedJob->getPoints(), $newPoints);
    }

    ///@override
    public function testModelInvalidSave(){
        $job = $this->getJob();

        //--------------------------------------------
        // Test Job Title Validation
        //--------------------------------------------
        $job->setTitle("#@!");
        try{
            $job->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test FieldID Validation
        //--------------------------------------------
        $job->setFieldID("#@!");
        try{
            $job->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test WorkerID Validation
        //--------------------------------------------
        $job->setWorkerID("#@!");
        try{
            $job->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test CityID Validation
        //--------------------------------------------
        $job->setCityID("#@!");
        try{
            $job->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test Points Validation
        //--------------------------------------------
        $job->setPoints("#@!");
        try{
            $job->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test Status Validation
        //--------------------------------------------
        $job->setStatus("#@!");
        try{
            $job->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test Description Validation
        //--------------------------------------------
//        $job->setDescription("#@!");
//        try{
//            $job->save();
//        }catch(Xception $e){
//            $this->assertEquals($e->getCode(), 10);
//        }
    }

    public function testGetApplicationState(){
        $job = $this->getJob();
        $userID = 2;
        $this->assertEquals($job->getApplicationState($userID), $job::$JOB_APP_HIRED);
    }

    public function testApply(){
        $jobID = 14;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 2;
        $job->apply($userID);
        $this->assertEquals($job->getApplicationState($userID), $job::$JOB_APP_PENDING);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 31
     */
    public function testForceClosedJobApplication(){
        $job = $this->getJob();
        $userID = 7;
        $job->apply($userID);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 31
     */
    public function testForceCreatedJobApplication(){
        $jobID = 14;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 1;
        $job->apply($userID);
    }

    public function testApplicationStateOfNoneApplicant(){
        $userID = 8;
        $job = $this->getJob();
        $this->assertNull($job->getApplicationState($userID));
    }

    public function testHireWorker(){
        $jobID = 17;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 1;
        $job->hireWorker($userID);

        $this->assertEquals($job->getWorkerID(), $userID);
        $this->assertEquals($job->getApplicationState($userID), Job::$JOB_APP_HIRED);
        $this->assertEquals($job->getStatus(), Job::$JOB_PENDING);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 10
     */
    public function testHireInvalidWorker(){
        $jobID = 17;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 5;
        $job->hireWorker($userID);
    }

    public function testFireWorker(){
        $jobID = 15;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 2;
        $job->fireWorker($userID);

        $this->assertNull($job->getWorkerID());
        $this->assertEquals($job->getApplicationState($userID), Job::$JOB_APP_FIRED);
        $this->assertEquals($job->getStatus(), Job::$JOB_AVAILABLE);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 10
     */
    public function testFireInvalidWorker(){
        $jobID = 17;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 5;
        $job->fireWorker($userID);
    }

    public function testQuitJob(){
        $jobID = 15;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 2;
        $job->quitJob($userID);

        $this->assertNull($job->getWorkerID());
        $this->assertEquals($job->getApplicationState($userID), Job::$JOB_APP_QUIT);
        $this->assertEquals($job->getStatus(), Job::$JOB_AVAILABLE);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 10
     */
    public function testQuitInvalidWorker(){
        $jobID = 17;
        $pdo = $this->getPdo();
        $job = Job::getByJobID($jobID, $pdo);
        $userID = 5;
        $job->quitJob($userID);
    }

    public function testGetAllByCreatorID(){
        $id = 1;
        $pdo = $this->getPdo();
        $result = Job::getAllByCreatorID($id, $pdo);
        $expect = $this->sqlSelectAll(
            "SELECT * FROM " . Job::$tableName . " WHERE creatorID = $id",
            'Job'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetAllByCreatorIDStatus(){
        $id = 1;
        $pdo = $this->getPdo();
        $status = Job::$JOB_PENDING;
        $result = Job::getAllByCreatorIDStatus($id, $status, $pdo);
        $expect = $this->sqlSelectAll(
            "SELECT * FROM " . Job::$tableName . " WHERE creatorID = $id AND status = '$status'",
            'Job'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetAllByWorkerID(){
        $id = 2;
        $pdo = $this->getPdo();
        $result = Job::getAllByWorkerID($id, $pdo);
        $expect = $this->sqlSelectAll(
            "SELECT * FROM " . Job::$tableName . " WHERE workerID = $id",
            'Job'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetAllByWorkerIDStatus(){
        $id = 2;
        $pdo = $this->getPdo();
        $status = Job::$JOB_PENDING;
        $result = Job::getAllByWorkerIDStatus($id, $status, $pdo);
        $expect = $this->sqlSelectAll(
            "SELECT * FROM " . Job::$tableName . " WHERE workerID = $id AND status = '$status'",
            'Job'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetAllByUserIDApplicationState(){
        $id = 1;
        $pdo = $this->getPdo();
        $status = Job::$JOB_PENDING;
        $result = Job::getAllByUserIDApplicationState($id, $status, $pdo);
        $this->assertEquals(count($result), 1);
    }

    public function testGetAllAvailable(){
        $pdo = $this->getPdo();
        $status = Job::$JOB_AVAILABLE;
        $result = Job::getAllAvailable($pdo);
        $expect = $this->sqlSelectAll(
            "SELECT * FROM " . Job::$tableName . " WHERE  status = '$status'",
            'Job'
        );
        $this->assertEquals($result, $expect);
    }

    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "job";
        $result = Job::findValue($value, $pdo);
        $expect = $this->sqlSelectAll(null, 'Job');
        $this->assertEquals($result, $expect);
    }
}