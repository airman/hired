<?php

require_once MODEL_DIR . 'Skill.php';

class SkillTest extends ModelTest
{
    public $modelName = 'Skill';
    protected $skill = null;

    public function getSkill()
    {
        if(!$this->skill){
            $id = 7;
            $pdo = $this->getPdo();
            $this->skill = Skill::getBySkillID($id, $pdo);
        }
        return $this->skill;
    }

    public function validModelProvider()
    {
        return array(
            array(
                array(
                    'name' => 'Test Skill', 
                    'fieldID' =>'4'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array(
                'Invalid name' => array(
                    'name' => '<!@ in#', 
                    'fieldID' =>'4'
                ),
                'Invalid fieldID' => array(
                    'name' => 'Test Skill', 
                    'fieldID' =>'1A'
                )
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing fieldID' => array(
                    'name' => 'Test Skill', 
                ),
                'Missing name' => array(
                    'skillID' => '55', 
                    'fieldID' =>'1'
                )
            )
        );
    }

    public function testModelSave(){
        $pdo = $this->getPdo();
        $skill = $this->getSkill();

        //--------------------------------------------
        // Test Getters
        //--------------------------------------------
        $this->assertEquals($skill->getName(), 'SkillA');
        $this->assertEquals($skill->getFieldID(), '4');
        
        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------
        $newName = "BROOM";
        $newFieldID = 8;
        $skill->setName($newName);
        $skill->setFieldID($newFieldID);
        $skill->save();
        $id = $skill->getSkillID();
        $updatedSkill = Skill::getByName($newName, $pdo);
        $this->assertEquals($updatedSkill->getName(), $newName);
        $this->assertEquals($updatedSkill->getFieldID(), $newFieldID);
    }

    public function testModelInvalidSave()
    {
        $skill = $this->getSkill();

        //--------------------------------------------
        // Test Skill Name Validation
        //--------------------------------------------
        $skill->setName("#@!");
        try {
            $skill->save();
        } catch (Xception $e) {
            $this->assertEquals($e->getCode(), 10);
        }

        //--------------------------------------------
        // Test FieldID Validation
        //--------------------------------------------
        $skill->setFieldID("#@!");
        try {
            $skill->save();
        } catch (Xception $e) {
            $this->assertEquals($e->getCode(), 10);
        }
    }

    public function testSetUserSkills(){
        $pdo = $this->getPdo();
        $id = 1;
        $expect = array('SkillA', 'SkillB', 'SkillC');
        Skill::setUserSkills($id, $expect, $pdo);
        $result = Skill::getAllByUserID($id, $pdo);
        $this->assertEquals(count($result), count($expect));
        $this->assertEquals($result[0]->getName(), $expect[0]);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 10
     */
    public function testSetInvalidUserSkills(){
        $pdo = $this->getPdo();
        $id = 1;
        $expect = array('SkillZ', 'SkillX', 'SkillV');
        Skill::setUserSkills($id, $expect, $pdo);
    }

    public function testSetJobSkills(){
        $pdo = $this->getPdo();
        $id = 14;
        $expect = array('SkillA', 'SkillB', 'SkillC');
        Skill::setJobSkills($id, $expect, $pdo);
        $result = Skill::getAllByJobID($id, $pdo);
        $this->assertEquals(count($result), count($expect));
        $this->assertEquals($result[0]->getName(), $expect[0]);
    }

    /**
     * @expectedException Xception
     * @expectedExceptionCode 10
     */
    public function testSetInvalidJobSkills(){
        $pdo = $this->getPdo();
        $id = 14;
        $expect = array('SkillZ', 'SkillG', 'SkillD');
        Skill::setJobSkills($id, $expect, $pdo);
    }

    public function testGetAllByUserIDFieldName(){
        $pdo = $this->getPdo();
        $id = 1;
        $fieldName = 'FieldA';
        $result = Skill::getAllByUserIDFieldName($id, $fieldName, $pdo);
        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->getSkillID(), 7);
        $this->assertEquals($result[0]->getFieldID(), 4);
        $this->assertEquals($result[0]->getName(), 'SkillA');
    }

    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "skill";
        $result = Skill::findValue($value, $pdo);
        $expect = $this->sqlSelectAll(null, 'Skill');
        $this->assertEquals($result, $expect);
    }

}
