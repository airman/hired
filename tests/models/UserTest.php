<?php
    
//require 'ModelTest.php';
require_once MODEL_DIR . 'User.php';

class UserTest extends ModelTest
{

    public $modelName = 'User';
    protected $user = null;

    public function getUser(){
        if(!$this->user){
            $id = 1;
            $pdo = $this->getPdo();
            $this->user = User::getByUserID($id, $pdo);
        }
        return $this->user;
    }

    public function validModelProvider()
    {
        return array(
            array(
                array(
                    'username' => 'Mark.Emps', 
                    'email' => 'empsworth@test.com', 
                    'password' =>'magic'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array( 'Invalid username' => array(
                    'username' => 'Mark Emps', 
                    'email' => 'empsworth@test.com', 
                    'password' =>'magic'
                ),
                'Invalid email' => array(
                    'username' => 'Mark Emps', 
                    'email' => 'empsworth.test.com', 
                    'password' =>'magic'
                ),
                'Invalid password' => array(
                    'username' => 'Mark Emps', 
                    'email' => 'empsworth@test.com', 
                    'password' =>'a'
                )
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing username' => array(
                    'email' => 'empsworth@test.com', 
                    'password' =>'magic'
                ),
                'Missing email' => array(
                    'username' => 'Mark Emps', 
                    'password' =>'magic'
                ),
                'Missing password' => array(
                    'username' => 'Mark Emps', 
                    'email' => 'empsworth@test.com', 
                )
            )
        );
    }

    /**
     * Test creation of Model using valid input
     * If successful, a new Model should be created, and should
     * be present in the database table.
     * @return null
     */
    public function testModelSave()
    {
        $pdo = $this->getPdo();
        $user = User::getByEmailPass(
            'dinesh@test.com',
            'password',
            $pdo
        );
        $this->assertTrue($user != null);

        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------
        $userID = $user->getUserID();
        $new_email = "danish@test.com";
        $new_pass = "wordpass";
        $user->setEmail($new_email);
        $user->setPassword($new_pass);
        $user->save();
        $user= null;
        $updatedUser = User::getByEmailPass(
            $new_email,
            $new_pass,
            $pdo
        );
        $this->assertTrue($updatedUser != null);
        $this->assertEquals($userID, $updatedUser->getUserID());
    }

    public function testModelInvalidSave(){
        $user = $this->getUser();

        //--------------------------------------------
        // Test username validation
        //--------------------------------------------
        $user->setUsername(" goomo $");
        try{
            $user->save();
            $this->fail("Username validation failed");
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test email validation
        //--------------------------------------------
        $user->setEmail(" goomo $");
        try{
            $user->save();
            $this->fail("Email validation failed");
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test password validation
        //--------------------------------------------
        $user->setPassword("a");
        try{
            $user->save();
            $this->fail("Password validation failed");
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 
    }

    public function testGetContactNumbers(){
        $user = $this->getUser();
        $result = $user->getContactNumbers();
        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->contactInfo, '0123456789');
    }

    public function testGetContactEmails(){
        $user = $this->getUser();
        $result = $user->getContactEmails();
        $this->assertEquals(count($result), 1);
        $this->assertEquals($result[0]->contactInfo, 'danish.dinesh@test.com');
    }


    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "dinesh";
        $result = User::findValue($value, $pdo);
        $this->assertEquals(count($result), 1);
    }
}
