<?php

require_once MODEL_DIR . 'Profile.php';

class ProfileTest extends ModelTest
{
    public $modelName = 'Profile';
    protected $profile = null;

    public function getProfile(){
        if(!$this->profile){
            $id = 1;
            $pdo = $this->getPdo();
            $this->profile = Profile::getByUserID($id, $pdo);
        }
        return $this->profile;
    }

    public function validModelProvider(){
        return array(
            array(
                array(
                    'userID' => '55', 
                    'firstName' => 'Lira', 
                    'lastName' =>'Lings',
                    'gender' =>'F'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array( 'Invalid first name' => array(
                        'userID' => '55', 
                        'firstName' => 'Li*ra', 
                        'lastName' =>'Lings',
                        'gender' =>'F'
                        ),
                    'Invalid last name' => array(
                        'userID' => '55', 
                        'firstName' => 'Lira', 
                        'lastName' =>'L*ngs',
                        'gender' =>'F'
                        ),
                    'Invalid userID' => array(
                        'userID' => 'hey', 
                        'firstName' => 'Lira', 
                        'lastName' =>'Lings',
                        'gender' =>'F'
                        ),
                    'Invalid gender' => array(
                        'userID' => '55', 
                        'firstName' => 'Lira', 
                        'lastName' =>'Lings',
                        'gender' =>'Z'
                        ),
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing userID' => array(
                        'firstName' => 'Lira', 
                        'lastName' =>'Lings',
                        'gender' =>'F'
                        ),
                'Missing first name' => array(
                        'userID' => 'hey', 
                        'lastName' =>'Lings',
                        'gender' =>'F'
                        ),
                'Missing last name' => array(
                        'userID' => 'hey', 
                        'firstName' => 'Lira', 
                        'gender' =>'F'
                        ),
                'Missing gender' => array(
                        'userID' => 'hey', 
                        'firstName' => 'Lira', 
                        'lastName' =>'Lings'
                        ),
            )
        );
    }

    /**
     * Test creation of Model using valid input
     * If successful, a new Model should be created, and should
     * be present in the database table.
     * @return null
     */
    public function testModelSave()
    {
        $pdo = $this->getPdo();
        $profile = $this->getProfile();
        $id = $profile->getUserID();

        //--------------------------------------------
        // Test Getters
        //--------------------------------------------
        $this->assertEquals($profile->getGenderName(), 'Male');
        $this->assertEquals($profile->getFullName(), 'Dinesh Naidoo');
        
        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------

        $newFirstName = "Lana"; 
        $newLastName = "Marbles"; 
        $newGender = "F"; 
        $newPoints = 19; 
        $newBio = "Lana lane, reporting for duty"; 
        $newAvatar = "lana.jpg"; 
        $profile->setFirstName($newFirstName);
        $profile->setLastName($newLastName);
        $profile->setGender($newGender);
        $profile->setPoints($newPoints);
        $profile->setBio($newBio);
        $profile->setAvatar($newAvatar);
        $profile->save();
        $this->assertEquals($profile->getGenderName(), 'Female');
        $profile = null;
        $updatedProfile = Profile::getByUserID($id, $pdo);
        $this->assertEquals($updatedProfile->getFirstName(), $newFirstName);
        $this->assertEquals($updatedProfile->getLastName(), $newLastName);
        $this->assertEquals($updatedProfile->getGender(), $newGender);
        $this->assertEquals($updatedProfile->getBio(), $newBio);
        $this->assertEquals($updatedProfile->getAvatar(), $newAvatar);
        $this->assertEquals($updatedProfile->getPoints(), $newPoints);
    }

    public function testModelInvalidSave(){
       $profile = $this->getProfile(); 

        //--------------------------------------------
        // Test First Name Validation
        //--------------------------------------------
        $profile->setFirstName("#@!"); 
        try{
            $profile->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test Last Name Validation
        //--------------------------------------------
        $profile->setLastName("#@!"); 
        try{
            $profile->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test Gender Validation 
        //--------------------------------------------
        $profile->setGender("E");
        try{
            $profile->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test Avatar Validation
        //--------------------------------------------
        $profile->setAvatar("#@!"); 
        try{
            $profile->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test Points Validation
        //--------------------------------------------
        $profile->setPoints("#@!"); 
        try{
            $profile->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test Bio Validation
        //--------------------------------------------
        //$profile->setFirstName("#@!"); 
        //try{
            //$profile->save();
        //}catch(Xception $e){
            //$this->assertEquals($e->getCode(), 10);
        //} 

    }

    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "dinesh";
        $result = Profile::findValue($value, $pdo);
        $this->assertEquals(count($result), 1);
    }
}
