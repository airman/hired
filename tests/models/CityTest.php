<?php

require_once MODEL_DIR . 'City.php';

class CityTest extends ModelTest{
    public $modelName = 'City';
    protected $city = null;

    public function getCity()
    {
        if(!$this->city){
            $id = 11;
            $pdo = $this->getPdo();
            $this->city = City::getByCityID($id, $pdo);
        }
        return $this->city;
    }

    public function validModelProvider()
    {
        return array(
            array(
                array(
                    'name' => 'Test City', 
                    'provinceID' =>'10'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array(
                'Invalid name' => array(
                    'name' => '<!@ in#', 
                    'provinceID' =>'10'
                ),
                'Invalid provinceID' => array(
                    'name' => 'Test City', 
                    'provinceID' =>'1A'
                )
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing provinceID' => array(
                    'name' => 'Test City', 
                ),
                'Missing name' => array(
                    'provinceID' =>'10'
                )
            )
        );
    }

  /**
   * Test duplicate data entry
   * If successful, no Model should be created
   * @param $record = array record to be used to create model
   *
   * @expectedException Xception
   * @expectedExceptionCode 10
   * @dataProvider validModelProvider
   **/
    public function testDuplicateModelCreation($record)
    {
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $modelName::create($record, $pdo);
        $modelName::create($record, $pdo);
    }


    public function testModelSave(){
        $pdo = $this->getPdo();
        $city = $this->getCity();

        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------
        $newName = "BROOM";
        $newProvinceID = 8;
        $this->assertNotEquals($city->getName(), $newName);
        $this->assertNotEquals($city->getProvinceID(), $newProvinceID);

        $city->setName($newName);
        $city->setProvinceID($newProvinceID);
        $city->save();
        $id = $city->getCityID();
        $updatedCity = City::getByName($newName, $pdo);
        $this->assertEquals($updatedCity->getName(), $newName);
        $this->assertEquals($updatedCity->getProvinceID(), $newProvinceID);
        $this->assertEquals($updatedCity->getCityID(), $id);
    }

    public function testModelInvalidSave(){
        $city = $this->getCity();

        //--------------------------------------------
        // Test City Name Validation
        //--------------------------------------------
        $city->setName("#@!"); 
        try{
            $city->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 

        //--------------------------------------------
        // Test ProvinceID Validation
        //--------------------------------------------
        $city->setProvinceID("#@!"); 
        try{
            $city->save();
        }catch(Xception $e){
            $this->assertEquals($e->getCode(), 10);
        } 
    }

    public function testGetByName(){
        $name = 'CityA';
        $pdo = $this->getPdo();
        $result = City::getByName($name, $pdo);
        $expect = $this->sqlSelectSingle(
            "SELECT * FROM " . City::$tableName . " WHERE name = '$name'",
            'City'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetAllByUserID(){
        $id = 1;
        $pdo = $this->getPdo();
        $result = City::getAllByUserID($id, $pdo);
        $expect = $this->sqlSelectAll(
            "SELECT * FROM user_locations WHERE userID = $id"
        );
        $this->assertEquals(count($result), count($expect));
    }

    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "city";
        $result = City::findValue($value, $pdo);
        $expect = $this->sqlSelectAll(null, 'City');
        $this->assertEquals($result, $expect);
    }
}
