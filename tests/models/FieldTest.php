<?php

require_once MODEL_DIR . 'Field.php';

class FieldTest extends ModelTest {
    public $modelName = 'Field';
    protected $field = null;

    public function getField()
    {
        if(!$this->field){
            $id = 4;
            $pdo = $this->getPdo();
            $this->field = Field::getByFieldID($id, $pdo);
        }
        return $this->field;
    }

    public function validModelProvider()
    {
        return array(
            array(
                array(
                    'name' => 'Test Field'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array(
                'Invalid name' => array(
                    'name' => '<!@ in#'
                )
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing name' => array(
                )
            )
        );
    }

    public function testModelSave(){
        $pdo = $this->getPdo();
        $field = $this->getField();

        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------
        $newName = "BROOM";
        $this->assertNotEquals($field->getName(), $newName);
        $field->setName($newName);
        $field->save();
        $id = $field->getFieldID();
        $updatedField = Field::getByName($newName, $pdo);
        $this->assertEquals($updatedField->getFieldID(), $id);
    }

    public function testModelInvalidSave()
    {
        $field = $this->getField();
        $field->setName("#@!");
        try {
            $field->save();
            $this->fail("Saved invalid field name");
        } catch (Xception $e) {
            $this->assertEquals($e->getCode(), 10);
        }
    }

    public function testGetAllByUserID(){
        $id = 1;
        $pdo = $this->getPdo();
        $result = Field::getAllByUserID($id, $pdo);
        $this->assertEquals(count($result), 2);
    }

    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "field";
        $result = Field::findValue($value, $pdo);
        $expect = $this->sqlSelectAll(null, 'Field');
        $this->assertEquals($result, $expect);
    }
}