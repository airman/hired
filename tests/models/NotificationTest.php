<?php

require_once MODEL_DIR . 'Notification.php';

class NotificationTest extends ModelTest {
    public $modelName = 'Notification';
    protected $notification = null;

    public function getNotification()
    {
        if(!$this->notification){
            $id = 18;
            $pdo = $this->getPdo();
            $this->notification = Notification::getByNotificationID($id, $pdo);
        }
        return $this->notification;
    }

    public function validModelProvider()
    {
        return array(
            array(
                array(
                    'message' => 'Test Notification',
                    'userID' =>'10'
                )
            )
        );
    }

    public function invalidModelProvider()
    {
        return array(
            array(
                'Invalid userID' => array(
                    'userID' => '<!@ in#',
                    'message' => 'Test Notification'
                )
            )
        );
    }

    public function incompleteModelProvider()
    {
        return array(
            array(
                'Missing userID' => array(
                    'message' => 'Test Notification'
                ),
                'Missing message' => array(
                    'userID' =>'10'
                )
            )
        );
    }

    /**
     * Test duplicate data entry
     * If successful, no Model should be created
     * @param $record = array record to be used to create model
     *
     * @dataProvider validModelProvider
     **/
    public function testDuplicateModelCreation($record)
    {
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $modelName::create($record, $pdo);
        $modelName::create($record, $pdo);
    }

    public function testModelSave(){
        $pdo = $this->getPdo();
        $notification = $this->getNotification();

        //--------------------------------------------
        // Test Model::save() method
        //--------------------------------------------
        $newMessage = "BROOM";
        $this->assertNotEquals($notification->getMessage(), $newMessage);
        $notification->setMessage($newMessage);
        $notification->save();
        $id = $notification->getNotificationID();
        $updatedNotification = Notification::getByNotificationID($id, $pdo);
        $this->assertEquals($updatedNotification->getMessage(), $newMessage);
    }

    public function testModelInvalidSave()
    {
    }

    public function testGetAllByUserID(){
        $id = 1;
        $pdo = $this->getPdo();
        $result = Notification::getAllByUserID($id, $pdo);
        $table = Notification::$tableName;
        $expect = $this->sqlSelectAll(
            "SELECT * FROM $table WHERE userID = $id",
            'Notification'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetAllByUserIDUnread(){
        $id = 1;
        $pdo = $this->getPdo();
        $unread = Notification::$UNREAD;
        $result = Notification::getAllByUserIDUnread($id, $unread, $pdo);
        $table = Notification::$tableName;
        $expect = $this->sqlSelectAll(
            "SELECT * FROM $table WHERE userID = $id AND unread = $unread",
            'Notification'
        );
        $this->assertEquals($result, $expect);
    }

    public function testGetNotificationCount(){
        $id = 1;
        $pdo = $this->getPdo();
        $result = Notification::getUserNotificationCount($id, $pdo);
        $this->assertEquals($result, 2);
    }

    public function testMarkAllUnread(){
        $id = 2;
        $pdo = $this->getPdo();

        //----------------------------------------
        // Before all read
        //----------------------------------------
        $result = Notification::getUnreadUserNotificationCount($id, $pdo);
        $this->assertEquals($result, 1);

        //----------------------------------------
        // After all read
        //----------------------------------------
        Notification::markAllRead($id, $pdo);
        $result = Notification::getUnreadUserNotificationCount($id, $pdo);
        $this->assertEquals($result, 0);
    }


    public function testFindValue(){
        $pdo = $this->getPdo();
        $value = "job";
        $result = Notification::findValue($value, $pdo);
        $expect = $this->sqlSelectAll(null, 'Notification');
//        $this->assertEquals($result, $expect);
    }
}