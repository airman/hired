<?php
/**
 *  Base class for Model Unit Tests,
 *  includes test methods for generic Model functions
 **/
abstract class ModelTest extends PHPUnit_Framework_TestCase{
    public $modelName = null;
    static public $pdo = null;
    public $deleteStatement = null;

    public function getPdo(){
        if(!ModelTest::$pdo) {
            ModelTest::$pdo = newTestPDO();
        }
        return ModelTest::$pdo;
    }

    public function setUp(){
        $pdo = $this->getPdo();
        $pdo->beginTransaction();
    }

    public function tearDown(){
        $pdo = $this->getPdo();
        $pdo->rollBack();
    }

    public function sqlSelectSingle($query=null, $fetchType=PDO::FETCH_OBJ){
        if(!$query) {
            $modelName = $this->modelName;
            $query = "SELECT * FROM " . $modelName::$tableName;
        }
        $pdo = $this->getPdo();
        $stmnt = $pdo->query($query);
        $stmnt->execute();
        if(gettype($fetchType) == 'string')
            return $stmnt->fetchAll(PDO::FETCH_CLASS, $fetchType, array($pdo))[0];
            # for some reason this line doesn't work
//            return $stmnt->fetch(PDO::FETCH_CLASS, $fetchType, array($pdo));
        return $stmnt->fetch($fetchType);
    }

    public function sqlSelectAll($query=null, $fetchType=PDO::FETCH_OBJ){
        if(!$query) {
            $modelName = $this->modelName;
            $query = "SELECT * FROM " . $modelName::$tableName;
        }
        $pdo = $this->getPdo();
        $stmnt = $pdo->query($query);
        $stmnt->execute();
        if(gettype($fetchType) == 'string')
            return $stmnt->fetchAll(PDO::FETCH_CLASS, $fetchType, array($pdo));
        return $stmnt->fetchAll($fetchType);
    }

  /**
   * Test creation of Model using valid input, and then test
   * toJSON() and delete() of said model
   * If successful, a new Model should be created, and should
   * be present in the database table.
   *
   * @param $record array record to be used to create model
   *
   * @return null
   *
   * @dataProvider validModelProvider
   **/
    public function testValidModelCreation($record)
    {
        //--------------------------------------------
        // Test model creation
        //--------------------------------------------
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $model = $modelName::create($record, $pdo);
        $this->assertTrue($model != null, "Valid model creation failed");
        $query = "SELECT * FROM " . $modelName::$tableName . " WHERE";

        //--------------------------------------------
        // Test Model::toJSON() method
        //--------------------------------------------
        $json_str = $model->toJSON();
        $json_obj = json_decode($json_str);

        foreach($modelName::$publicAttributes as $attr){
            $value = $model->{"get" . ucfirst($attr)}();
            $this->assertEquals($json_obj->{$attr}, $value);
            // prepare query for delete assertion
            $query .= " $attr = " . $pdo->quote($value) . " AND";
        }
        $query = substr($query, 0, strlen($query) - strlen(" AND"));

        //--------------------------------------------
        // Test Model::delete() method
        //--------------------------------------------
        $model->delete();
        $this->assertFalse($this->sqlSelectSingle($query));
    }

  /**
   * Test duplicate data entry
   * If successful, no Model should be created however, this
   * test will be overridden should a specific model allow
   * duplicate record data
   * @param $record = array record to be used to create model
   *
   * @expectedException Xception
   * @expectedExceptionCode 13
   * @dataProvider validModelProvider
   **/
    public function testDuplicateModelCreation($record)
    {
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $modelName::create($record, $pdo);
        $modelName::create($record, $pdo);
    }

  /**
   * Test creation of Model using invalid input
   * If successful, a new Model should not be created, and no 
   * data should be present in the database table.
   * @param $record = array record to be used to create model
   *
   * @dataProvider invalidModelProvider
   * @expectedException Xception
   * @expectedExceptionCode 10
   **/
    public function testInvalidModelCreation($record)
    {
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $this->assertNull($modelName::create($record, $pdo), "Invalid model created");
        $this->assertFalse($this->sqlSelectSingle());
    }
    

  /**
   * Test creation of Model using incomplete input
   * If successful, a new Model should not be created, and no 
   * data should be present in the database table.
   * @param $record = array record to be used to create model
   *
   * @dataProvider incompleteModelProvider
   * @expectedException Xception
   * @expectedExceptionCode 11
   **/
    public function testIncompleteModelCreation($record)
    {
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $this->assertNull($modelName::create($record, $pdo), "Incomplete model created");
        $this->assertFalse($this->sqlSelectSingle());
    }


    public function testGetAll(){
        $modelName = $this->modelName;
        $pdo = $this->getPdo();
        $result = $modelName::getAll($pdo);
        $expect = $this->sqlSelectAll(null, $modelName);
        $this->assertEquals($result, $expect);
    }

    /// test Model::save() method
    public abstract function testModelSave();

    /// test Model::save() method but save invalid data
    public abstract function testModelInvalidSave();

    /// test Model::findValue() method
    public abstract function testFindValue();

    /// provides input for testValidModelCreation method
    public abstract function validModelProvider();

    /// provides input for testInvalidModelCreation method
    public abstract function invalidModelProvider();

    /// provides input for testIncompleteModelCreation method
    public abstract function incompleteModelProvider();

}

