use hired_db;

INSERT INTO fields (name, description) VALUES 
    ("IT", "Computers etc"),
    ("Motor Vehicle", "Motor Vehicles etc"),
    ("Electrical Appliance", "Electrical Appliances etc"),
    ("Hair", "Hair etc")
;

INSERT INTO locations (name) VALUES
    ("Germiston"),
    ("Alberton"),
    ("Dinwiddie")
;
