drop table if exists users;
create table users(
    userID int primary key auto_increment not null,
    username varchar(45) unique not null,
    email varchar(40) unique not null,
    password varchar(32) not null,
    created datetime not null default current_timestamp,
    groupID int not null default 1
);

drop table if exists profiles;
create table profiles(
    userID int primary key references users(userID),
    firstName varchar(20) not null,
    lastName varchar(20) not null,
    gender varchar(1) not null,
    avatar varchar(35) not null,
    bio varchar(150) not null,
    points int not null default 5
);

drop table if exists provinces;
create table provinces(
    provinceID int primary key auto_increment not null,
    name varchar(50) not null unique
);

drop table if exists cities;
create table cities(
    cityID int primary key auto_increment not null,
    name varchar(50) not null,
    provinceID int not null references provinces(provinceID)
);

drop table if exists fields;
create table fields(
    fieldID int primary key auto_increment not null,
    name varchar(20) not null unique,
    description text not null
);

drop table if exists skills;
create table skills(
    skillID int primary key auto_increment not null,
    name varchar(30) not null unique,
    fieldID int not null references fields(fieldID)
);

drop table if exists jobs;
create table jobs(
    jobID int primary key auto_increment not null,
    creatorID int not null references users(userID),
    workerID int references users(userID),
    fieldID int not null references fields(fieldID),
    cityID int not null references cities(cityID),
    title varchar(40) not null,
    description text not null,
    created datetime not null default current_timestamp,
    points int not null default 0,
    status varchar(1) not null default 'A'
);

drop table if exists user_contacts;
create table user_contacts(
    userID int not null references users(userID),
    contactInfo varchar(50) not null,
    contactType varchar(1) not null
);

drop table if exists user_locations;
create table user_locations(
    userID int not null references users(userID),
    cityID int not null references cities(cityID)
);

drop table if exists user_skills;
create table user_skills(
    userID int not null references users(userID),
    skillID int not null references skills(skillID)
);

drop table if exists job_skills;
create table job_skills(
    jobID int not null references jobs(jobID),
    skillID int not null references skills(skillID)
);

drop table if exists user_notifications;
create table user_notifications(
    notificationID int primary key auto_increment not null,
    userID int not null references users(userID),
    message varchar(100) not null,
    created datetime not null default current_timestamp,
    unread boolean not null default true
);

drop table if exists job_applications;
create table job_applications(
    jobID int not null references jobs(jobID),
    userID int not null references users(userID),
    created datetime not null default current_timestamp,
    state varchar(1) not null default 'P'
);

drop table if exists user_connections;
create table user_connections(
    senderID int not null references users(userID),
    recipientID int not null references users(userID),
    created datetime not null default current_timestamp
);

drop table if exists connection_invitations;
create table connection_invitations(
    invitationID int primary key auto_increment not null,
    senderID int not null references users(userID),
    recipientID int not null references users(userID),
    created datetime not null default current_timestamp,
    state varchar(1) not null default 'P'
);
