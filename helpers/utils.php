<?php

    /**
     * @return null|PDO
     */
    function newPDO(){
        $pdo = null;
        $dsn = DSN;
        $dbhost = DB_HOST;
        $dbname = DB_NAME;
        $dbuser = DB_USER;
        $dbpass = DB_PASS;
        try{
            $pdo = new PDO("$dsn:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $e){
            # TODO log error
            setAlert($e->getMessage(), "alert-danger");
            header("Location: /?=signup");
        }
        return $pdo;
    }

    function setAlert($msg, $type){
        $_SESSION['alert_msg'] = $msg;
        $_SESSION['alert_type'] = $type;
    }

    function redirectedAlert($page, $msg, $type){
        setAlert($msg, $type);
        header("Location: $page");
    }

    function showError($msg, $code){
        $_SESSION['error_msg'] = $msg;
        $_SESSION['error_code'] = $code;
        header("Location: ?page=error");
    }

    /**
     * @return string
     */
    function getAlertHTML(){
        $html = '<div class="alert hidden"></div>';
        if(isset($_SESSION['alert_type'])){
            $html = sprintf(
                        '<div class="alert %s">%s</div>' . PHP_EOL,
                        $_SESSION['alert_type'],
                        $_SESSION['alert_msg']
                    );
        }
        $_SESSION['alert_msg'] = null;
        $_SESSION['alert_type'] = null;
        return $html;
    }

    /**
     * @param $user
     * @param $next
     * @param $fail
     */
    function login($user, $next, $fail){
        if($user == null){
           setAlert("Invalid login details", "alert-danger");
           header("Location: $fail");
           return;
        }
        $profile = Profile::getByUserID($user->getUserID(), $user->getPdo());
        if($profile == null){
            setAlert(
                "Sorry, but something unexpected happened, please try again.",
                "alert-warning");
            header("Location: $fail");
        }
        $_SESSION['user']['id'] = $user->getUserID();
        $_SESSION['user']['gid'] = $user->getGroupID();
        header("Location: $next");
    }

    /**
     * @param $next
     */
    function logout($next){
        session_destroy();
        $_SESSION = array();
        header("Location: $next");
    }

    /**
     * @return bool
     */
    function isLoggedIn(){
        return isset($_SESSION['user']);
    }


