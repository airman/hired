<?php
    /***
     * Returns true if the associative array 
     * passed in $assoc contains the keys passed
     * in array $keys
     *@param $keys = array of keys
     *@param $assoc = associative array
     *@return boolean
     ***/
    function arrayContainsKeys($keys, $assoc){
        return array_intersect($keys, array_keys($assoc)) == $keys;
    }

    /***
     * Returns true if the associative array 
     * passed in $assoc has all the keys passed
     * in array $keys set to non-null value
     *@param $keys = array of keys
     *@param $assoc = associative array
     *@return boolean
     ***/
    function arrayKeysSet($keys, $assoc){
        foreach($keys as $key){
            if(!isset($assoc[$key]))
                return false;
        } 
        return true;
    }

    /***
     * Returns existing form token if found, otherwise
     * creates, stores and returns new form token
     * @return string
     ***/
    function getFormToken(){
        if(!isset($_SESSION['form_token']))
            $_SESSION['form_token'] = md5(uniqid('auth', true));
        return $_SESSION['form_token'];
    }

    /***
     * Returns true if given token matches
     * the form token
     * @param $token = string token
     * @return boolean
     ***/
    function isFormTokenValid($token){
        return $token == getFormToken();
    }

    function isEmailValid($email){
        $emailReg = "/^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$/";
        return preg_match($emailReg, $email);
    }

    function isPhoneNumberValid($phone){
        return preg_match("/^[0-9]{10}$/", $phone);
    }

    function isUsernameValid($username){
        return preg_match("/^[0-9a-zA-Z_\.\-]{5,}$/", $username);
    }

    function isPasswordValid($username){
        return preg_match("/^.{5,}$/", $username);
    }

    function isNameValid($name){
        return preg_match("/^[a-zA-z\-]{3,}$/", $name);
    }

    function isSkillNameValid($name){
        return preg_match("/^[a-zA-z\s_+\-\d]{3,}$/", $name);
    }

    function isCityNameValid($name){
        return preg_match("/^[a-zA-z\s]{3,}$/", $name);
    }

    function isIDValid($id){
        return preg_match("/^[0-9]{1,}$/", $id);
    }

    function isGenderValid($gender){
        $g = strtolower($gender[0]);
        if($g == 'm' || $g == 'f')
            return true;
        return false;
    }

    function isJobStatusValid($status){
        return preg_match("/^[APC]{1}$/", $status);
    }
