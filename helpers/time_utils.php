<?php


/**
 * @param $date1
 * @param $date2
 * @return string
 */
    date_default_timezone_set("UTC");

function currentDate(){
    return date('Y-m-d H:i:s');
}

function getAge($date1, $date2){
    $date1_ = explode(" ", $date1);
    $date2_ = explode(" ", $date2);

    $date1_ymd = $date1_[0];
    $date2_ymd = $date2_[0];

    if($date1_ymd != $date2_ymd){
        $date1_ = explode('-', $date1_ymd);
        $date2_ = explode('-', $date2_ymd);
        if($date1_[0] == $date2_[0] && $date1_[1] == $date2_[1]){
            $delta_days = (int) $date2_[2] - (int) $date1_[2];
            if ($delta_days < 10)
                return "$delta_days day" . ($plural = ($delta_days > 1)? "s":"") . " ago";
        }
        return $date1_ymd;
    }

    $date2_time = $date2_[1];
    $date1_time = $date1_[1];
    $date1_time_exp = explode(":", $date1_time);
    $date2_time_exp = explode(":", $date2_time);
    $date1_hrs = $date1_time_exp[0];
    $date1_min = $date1_time_exp[1];
//    $date1_sec = $date1_time_exp[2];

    $date2_hrs = $date2_time_exp[0];
    $date2_min = $date2_time_exp[1];
//    $date2_sec = $date2_time_exp[2];

    $delta_hrs = (int) $date2_hrs - (int) $date1_hrs;
    $delta_min = (int) $date2_min - (int) $date1_min;
//    $delta_sec = (int) $date2_sec - (int) $date1_sec;

    if($delta_hrs != 0)
        return "$delta_hrs hour" . ($plural = ($delta_hrs > 1)? "s":"" ) . " ago";
    else if($delta_min != 0)
        return "$delta_min minute" . ($plural = ($delta_min > 1)? "s":"" ) . " ago";
    //return "$delta_sec second" . ($plural = ($delta_sec> 1)? "s":"" ) . " ago";
    return "just now";
}
