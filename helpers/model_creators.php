<?php

    include_once MODEL_DIR . "Skill.php";
    include_once MODEL_DIR . "City.php";

    function createSkills($fieldID, $skills, $pdo){
        foreach($skills as $skill){
            $record = array(
                'fieldID' => $fieldID,
                'name'    => $skill
            );
            $skillObj = Skill::create($record, $pdo);
            if($skillObj == null){
                # TODO log error
            }
        }
    }

    function createCity($cityName, $pdo){
        $city = City::getByName($cityName, $pdo);
        if($city)
            return $city->getCityID();
        $record = array(
            'name'    => $cityName,
            'provinceID' => 1
        );
        $city = City::create($record, $pdo);
        return $city->getCityID();
    }

    function createLocations($locations, $pdo){
        $count = 0;
        foreach($locations as $location){
            $location = trim($location);
            if(strlen($location) < 2)
                continue;
            if(Location::getByName($location, $pdo))
                continue;
            $record = array(
                'name'    => $location
            );
            $locationObj = Location::create($record, $pdo);
            if($locationObj == null){
                # TODO log error
                echo "something wrong<br/>";
                $count--;
            }
            $count+=1;
        }
    }
