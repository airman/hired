<?php
    /***
     * Returns string of alphabet characters from input string
     * @param $value = string
     * @return string
     ***/    
    function filterAlpha($value){
        return preg_replace('/^[a-zA-Z]$/', '', $value);
    }

    /***
     * Returns string of alphanumeric characters from input string
     * @param $value = string
     * @return string
     ***/    
    function filterAlphaNum($value){
        return preg_replace('/^[0-9a-zA-Z]$/', '', $value);
    }

    /***
     * Returns string of alphabet characters from input string
     * @param $value = string
     * @return int 
     ***/    
    function filterInt($value){
        return (int) preg_replace('/^[0-9]$/', '', $value);
    }

    /***
     * Returns float from input string
     * @param $value = string
     * @return float
     ***/    
    function filterFloat($value){
        $value = preg_replace('/^(.)(0-9)$/', '', $value);
        if($value){
            // remove excess decimal points
            $values = explode('.', $value);
            $values[0] += '.';
            return implode('', $values);
        }
        return (float) $value;
    }
