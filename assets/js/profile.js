
function getFieldIndex(field){
    for(i=0; i < arrFields.length; i++){
        if(arrFields[i] == field)
            return i;
    }
    return -1;
}

function hideProfile(){
	$("#profile-dialog").addClass('hidden');
	$("#overlay").addClass('hidden');			
}

function showDisplay(){
	$("#tabDisplay").removeClass("hidden");
	$("#tabBtnDisplay").addClass("active");
	$('#profile-dialog .tab-title').html('Display Info');
	hideSkills();
	hideContactInfo(); 
}

function hideDisplay(){
	$("#tabDisplay").addClass("hidden");
	$("#tabBtnDisplay").removeClass("active");
}

function showSkills(){
	$("#tabSkills").removeClass("hidden");
	$("#tabBtnSkills").addClass("active");
	$('#profile-dialog .tab-title').html('Add/Remove Skills');
	hideDisplay();
	hideContactInfo();
}

function hideSkills(){
	$("#tabSkills").addClass("hidden");
	$("#tabBtnSkills").removeClass("active");
}

function showContactInfo(){
	$("#tabContactInfo").removeClass("hidden");
	$("#tabBtnContactInfo").addClass("active");
	$('#profile-dialog .tab-title').html('Contacts and Locations');
	hideDisplay();
	hideSkills();
}

function hideContactInfo(){
	$("#tabContactInfo").addClass("hidden");
	$("#tabBtnContactInfo").removeClass("active");
}

function showAddLocation(){
	$("#dlgLocation").dialog('open');
}

function showAddEmail(){
	$("#dlgEmail").dialog('open');
}

function showAddPhone(){
	$("#dlgPhone").dialog('open');
}

function saveProfile(){
	hideProfile();
	alert.html("Profile updated.");
	alert.prop("class", "alert alert-success");
	alert.show();
	initAlerts();
}

function cancelProfile(){
	hideProfile();
}

function fieldClick(){
	var field = fieldSelector.val();
	fieldTitle.html(field + " skills:");
    var indx = getFieldIndex(field);
    $("#field-skills").val(arrSkills[indx]);
}

function populateFieldNames(){
    var html = "";
    for(i =0;i < arrFields.length; i++){
        html += "<option>" + arrFields[i] + "</option>\n";
    }
    $(".field-selector").html(html);
}

function populateContactInformation(){
    $("#phones").val(Phones);
    $("#emails").val(Emails);
    $("#locations").val(Locations);
}

function updateProfileDisplay(){
    firstName = $("#firstName").val();
    lastName = $("#firstName").val();
    txtBio = $("#txtBio").val();
    avatar = $("#avatar-upload").val();

    // add helpers to show user what's wrong
    if(firstName == "" || lastName == "")
        return; 
    //send display info
    data = {
        'part'     : 'display',
        'formToken': $('#formToken').val(),
        'firstName': firstName,
        'lastName' : lastName,
        'txtBio'   : txtBio
    };
    sendProfileUpdate(data);
}


function updateProfileSkills(){
    // send arrSkills
    data = {
        'part'     : 'skills',
        'formToken': $('#formToken').val(),
        'skills'   : fieldSkills
    };
    sendProfileUpdate(data);
    
}

function updateProfileContact(){
    // send arrContact
    phones = $("#phones").val(); 
    emails = $("#emails").val(); 
    locations = $("#locations").val(); 
    data = {
        'part'     : 'contact',
        'formToken': $('#formToken').val(),
        'phones': phones,
        'locations' : locations,
        'emails'   : emails
    };
    sendProfileUpdate(data);
}

function sendProfileUpdate(data){
    $.ajax({
        type    : 'POST',
        url     : 'localhost/hired/ajax/updateProfile.php',
        data    : data,
        success : profileUpdateResponse,
        dataType: 'json'
    });
}

function profileUpdateResponse(data){
}

function initProfile(){
    var options = {};
    options.ui = {
        showPopover: true,
        showErrors: false,
        showProgressBar: true
    };
    options.rules = {
        activated: {
            wordTwoCharacterClasses: false,
            wordRepetitions: false
        }
    };
    emailHelper = $("#email-helper");
    phoneHelper = $("#phone-helper");
    firstNameHelper = $("#first-name-helper");
    lastNameHelper = $("#last-name-helper");
    fieldSelector = $("#field-selector");
    fieldTitle = $("#field-title");
    helpers = $(".helper");
    alert = $(".alert");

	$("#tabBtnDisplay").click(showDisplay);
	$("#tabBtnSkills").click(showSkills);
	$("#tabBtnContactInfo").click(showContactInfo);
	$("#addPhone").click(showAddPhone);
	$("#addEmail").click(showAddEmail);
	$("#addLocation").click(showAddLocation);
	$("#btnCancel").click(cancelProfile);
	//$("#btnSave").click(saveProfile);
	$("#upload-caption").click(function(){$(".file-browser").click()});
	fieldSelector.click(fieldClick);
    populateFieldNames();
    populateContactInformation();
}
