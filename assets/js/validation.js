var emailPattern =  new RegExp(
	"^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$"
);//put this outside of function

var namePattern = new RegExp(
	"^[a-zA-Z\-]+$"
);

var skillsPattern = new RegExp(
	"^[a-zA-Z\-,]+$"
);

var phonePattern = new RegExp(
	"^[+][0-9]{11}");
function emailExists(){
	return false ;	
}

function isEmailValid(email){
	return emailPattern.test(email);
}

function isNameValid(name){
	return namePattern.test(name);
}
