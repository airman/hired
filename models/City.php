<?php

include_once CLASS_DIR . 'Model.php';

class City extends Model{

    protected $cityID;
    protected $name;
    protected $provinceID;

    public static $tableName = 'cities';
    public static $privateAttributes = array();
    public static $publicAttributes = array(
        'cityID',
        'name',
        'provinceID'
    );

    public static $recordAttributes = array(
        'name',
        'provinceID'
    );

    /// @override
    protected function setupStatements(){
        $pdo = $this->getPdo();
        # update
        $query = sprintf(
            'UPDATE %s SET %s WHERE cityID = :cityID',
            City::$tableName,
            'name = :name, provinceID = :provinceID'
        );
        $updateStatement = $pdo->prepare($query);
        $updateStatement->bindParam(
            ':name',
            $this->name,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':cityID',
            $this->cityID,
            PDO::PARAM_INT
        );
        $updateStatement->bindParam(
            ':provinceID',
            $this->provinceID,
            PDO::PARAM_INT
        );

        # delete
        $query = sprintf(
            'DELETE FROM %s WHERE cityID = :cityID',
            City::$tableName
        );
        $deleteStatement = $pdo->prepare($query);
        $deleteStatement->bindParam(
            ':cityID',
            $this->cityID,
            PDO::PARAM_INT
        );
        $this->updateStatement = $updateStatement;
        $this->deleteStatement = $deleteStatement;
    }

    /// @override
    public function isModelDataValid(){
        if(!isCityNameValid($this->name))
            return 'Invalid name';
        if(!isIDValid($this->provinceID))
            return 'Invalid provinceID';
        return parent::isModelDataValid();
    }


    /// @override
    protected static function insert($record, $pdo){
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            City::$tableName,
            'name, provinceID',
            ':name, :provinceID'
        );
        $stmnt = $pdo->prepare($query);
        $stmnt->bindValue(
            ':name',
            ucwords($record['name']),
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':provinceID',
            $record['provinceID'],
            PDO::PARAM_INT
        );
        $stmnt->execute();
    }

    /// @override
    protected static function isRecordValid($record, $pdo){
        if(!isCityNameValid($record['name']))
            return 'Invalid name';
        if(!isIDValid($record['provinceID']))
            return 'Invalid provinceID';
        return parent::isRecordValid($record, $pdo);
    }


    /// @override
    public static function create($record, $pdo){
        $city = City::getByName($record['name'], $pdo);
        if($city != null){
            if($city->getProvinceID() == $record['provinceID']){
                throw new Xception(
                    "Duplicate City",
                    Xception::$ERR_INVALID_DATA,
                    $record
                );
            }
        }
        parent::create_('City', $record, $pdo);
        $id = $pdo->lastInsertId();
        return City::getByCityID($id, $pdo);
    }

    /// @override
    public static function getAll($pdo){
        return parent::getAll_('City', $pdo);
    }

    /***
     * Returns a new Model with matching cityID
     * @param $cityID = int id
     * @param $pdo = PDO connection to be used when executing queries
     * @return City
     ***/
    public static function getByCityID($cityID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE cityID = :cityID',
                City::$tableName
            )
        );
        $stmnt->bindValue(':cityID', $cityID, PDO::PARAM_INT);
        return parent::getModelFromStatement('City', $stmnt, $pdo);
    }

    public static function getByName($cityName, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE name = :cityName',
                City::$tableName
            )
        );
        $stmnt->bindValue(':cityName', $cityName, PDO::PARAM_STR);
        return parent::getModelFromStatement('City', $stmnt, $pdo);
    }

    public static function getAllByUserID($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT c.cityID, c.name, c.provinceID FROM %s WHERE userID = :userID',
                City::$tableName . ' c INNER JOIN user_locations ul ON ' .
                'c.cityID = ul.cityID'
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        return parent::getModelsFromStatement('City', $stmnt, $pdo);
    }

    /**
     * @param $value = value to search for in all model column's
     * @param $pdo
     * @return mixed
     */
    public static function findValue($value, $pdo)
    {
        $table = City::$tableName;
        $stmnt = $pdo->prepare(
            "SELECT * FROM $table WHERE name LIKE :value"
        );
        $stmnt->bindValue(":value", "%$value%");
        return parent::getModelsFromStatement('City', $stmnt, $pdo);
    }


}
