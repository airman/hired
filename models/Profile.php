<?php
require_once CLASS_DIR . 'Model.php';

class Profile extends Model{
    protected $userID;
    protected $firstName;
    protected $lastName;
    protected $avatar;
    protected $gender;
    protected $bio;
    protected $points;

    public static $tableName = 'profiles';
    public static $privateAttributes = array();
    public static $publicAttributes = array(
                                            'userID',
                                            'firstName',
                                            'lastName',
                                            'avatar',
                                            'gender',
                                            'bio',
                                            'points'
                                        );
    public static $recordAttributes = array(
                                            'userID',
                                            'firstName',
                                            'lastName',
                                            'gender',
                                        );

    /// @override
    protected function setupStatements(){
        $pdo = $this->getPdo();
        # update
        $query = sprintf(
            'UPDATE %s SET %s WHERE userID = :userID',
            Profile::$tableName,
            'firstName = :firstName, lastName = :lastName, ' .
            'avatar = :avatar, gender = :gender, bio = :bio, ' .
            'points = :points'
        );
        $updateStatement = $pdo->prepare($query);
        $updateStatement->bindParam(
            ':userID',
            $this->userID,
            PDO::PARAM_INT
        );
        $updateStatement->bindParam(
            ':firstName',
            $this->firstName,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':lastName',
            $this->lastName,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':avatar',
            $this->avatar,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':gender',
            $this->gender,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':bio',
            $this->bio,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':points',
            $this->points,
            PDO::PARAM_INT
        );


        # delete
        $query = sprintf(
            'DELETE FROM %s WHERE userID = :userID',
            Profile::$tableName
        );
        $deleteStatement = $pdo->prepare($query);
        $deleteStatement->bindParam(
            ':userID',
            $this->userID,
            PDO::PARAM_INT
        );
        $this->updateStatement = $updateStatement;
        $this->deleteStatement = $deleteStatement;
    } 

    /// @override
    protected static function insert($record, $pdo){
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            Profile::$tableName,
            'userID, firstName, lastName, ' . 
            'gender, avatar, bio',
            ':userID, :firstName, :lastName, ' .
            ':gender, :avatar, :bio'
        );
        $stmnt = $pdo->prepare($query); 
        $stmnt->bindValue(
            ':userID',
            $record['userID'],
            PDO::PARAM_INT
        );
        $stmnt->bindValue(
            ':firstName',
            $record['firstName'],
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':lastName',
            $record['lastName'],
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':avatar',
            'avatar-default.svg', 
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':gender',
            $record['gender'],
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':bio',
            "Hi, I'm " . $record['firstName'] . 
            " and I'm ready to get hired.",
            PDO::PARAM_STR
        );
        $stmnt->execute();
    }

    /// @override
    protected static function isRecordValid($record, $pdo){
        if(!isNameValid($record['firstName']))
            return 'Invalid first name';
        if(!isNameValid($record['lastName']))
            return 'Invalid last name';
        if(!isIDValid($record['userID']))
            return 'Invalid userID';
        if(!isGenderValid($record['gender']))
            return 'Invalid gender';
        return parent::isRecordValid($record, $pdo);
    }

    /// @override
    public static function create($record, $pdo){
        parent::create_('Profile', $record, $pdo);
        return Profile::getByUserID(
            $record['userID'], 
            $pdo
        );
    }

    /// @override
    public static function getAll($pdo){
        return parent::getAll_('Profile', $pdo);
    }

    /***
     * Returns a new Model with matching userID
     * @param $userID = int id
     * @param $pdo = PDO connection to be used when executing queries
     * @return Profile 
     ***/
    public static function getByUserID($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE userID = :userID',
                Profile::$tableName
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        return parent::getModelFromStatement('Profile', $stmnt, $pdo);
    }

    /// @override
    public function isModelDataValid(){
        if(!isNameValid($this->firstName))
            return 'Invalid first name';
        if(!isNameValid($this->lastName))
            return 'Invalid last name';
        if(!isGenderValid($this->gender))
            return 'Invalid gender';
        if(!isUsernameValid($this->avatar))
            return 'Invalid avatar';
        if(!isIDValid($this->points))
            return 'Invalid points';
        //if(!isBioValid($record['']))
            //return 'Invalid bio';
        return parent::isModelDataValid();
    }

    public function getFullName(){
        return $this->firstName . ' ' . $this->lastName;
    }

    public function getGenderName(){
        return ($this->gender == 'M')? 'Male':'Female';
    }

    /**
     * @param $value = value to search for in all model column's
     * @param $pdo
     * @return mixed
     */
    public static function findValue($value, $pdo)
    {
        $table = Profile::$tableName;
        $stmnt = $pdo->prepare(
            "SELECT * FROM $table WHERE firstName LIKE :value OR lastName LIKE :value"
        );
        $stmnt->bindValue(":value", "%$value%");
        return parent::getModelsFromStatement('Profile', $stmnt, $pdo);
    }

}
