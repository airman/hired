<?php

include_once CLASS_DIR . 'Model.php';

class User extends Model{

    protected $userID;
    protected $username;
    protected $email;
    protected $password;
    protected $created; 
    protected $groupID;

    public static $tableName = 'users';
    public static $privateAttributes = array(
                                            'password',
                                            'userID',
                                        );
    public static $publicAttributes = array(
                                            'email',
                                            'created',
                                            'username',
                                            'groupID'
                                        );
    public static $recordAttributes = array(
                                            'email',
                                            'password',
                                            'username'
                                        );

    /// @override
    protected function setupStatements(){
        $pdo = $this->getPdo();
        # update
        $query = sprintf(
            'UPDATE %s SET %s WHERE userID = :userID',
            User::$tableName,
            'email = :email, password = :password, username = :username'
        );
        $updateStatement = $pdo->prepare($query);
        $updateStatement->bindParam(
            ':email',
            $this->email,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':password',
            $this->password, PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':username',
            $this->username, PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':userID',
            $this->userID,
            PDO::PARAM_INT
        );

        # delete
        $query = sprintf(
            'DELETE FROM %s WHERE userID = :userID',
            User::$tableName
        );
        $deleteStatement = $pdo->prepare($query);
        $deleteStatement->bindParam(
            ':userID',
            $this->userID,
            PDO::PARAM_INT
        );
        $this->updateStatement = $updateStatement;
        $this->deleteStatement = $deleteStatement;
    } 

    /// @override
    protected static function insert($record, $pdo){
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            User::$tableName,
            'username, email, password',
            ':username, :email, :password'
        );
        $stmnt = $pdo->prepare($query); 
        $stmnt->bindValue(
            ':username', 
            strtolower($record['username']),
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':email', 
            strtolower($record['email']),
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':password', 
            User::encrypt($record['password']),
            PDO::PARAM_STR
        );
        $stmnt->execute();
    }

    /// @override
    public function isModelDataValid(){
        if(!isUsernameValid($this->username))
            return 'Invalid username';
        if(!isEmailValid($this->email))
            return 'Invalid email';
        if(!isPasswordValid($this->password))
            return 'Invalid password';
        return parent::isModelDataValid();
    }

    /// @override
    protected static function isRecordValid($record, $pdo){
        if(!isUsernameValid($record['username']))
            return 'Invalid username';
        if(!isEmailValid($record['email']))
            return 'Invalid email address';
        if(!isPasswordValid($record['password']))
            return 'Invalid password';
        return parent::isRecordValid($record, $pdo);
    }

    /// @override
    public static function create($record, $pdo){
        parent::create_('User', $record, $pdo);
        $id = $pdo->lastInsertId();
        return User::getByUserID($id, $pdo);
    }

    /// @override
    public static function getAll($pdo){
        return parent::getAll_('User', $pdo);
    }

    /***
     * Returns a new Model with matching userID
     * @param $userID = int id
     * @param $pdo = PDO connection to be used when executing queries
     * @return User 
     ***/
    public static function getByUserID($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE userID = :userID',
                User::$tableName
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        return parent::getModelFromStatement('User', $stmnt, $pdo);
    }

    /***
     * Returns a new Model with matching email and password
     * @param $email = string email
     * @param $password = string unencrypted password 
     * @param $pdo = PDO connection to be used when executing queries
     * @return User 
     ***/
    public static function getByEmailPass($email, $password, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE %s', 
                User::$tableName,
                'email = :email AND ' .
                'password = :password'
            )
        );
        $stmnt->bindValue(':email', strtolower($email), PDO::PARAM_STR);
        $stmnt->bindValue(':password', User::encrypt($password), PDO::PARAM_STR);
        return parent::getModelFromStatement('User', $stmnt, $pdo);

    }

    /***
     * Returns encrypted phrase
     * @param $phrase = string to be encrypted
     * @return string
     ***/
    private static function encrypt($phrase){
        return md5($phrase);
    }

    public function setPassword($pass){
        $this->password = $this::encrypt($pass);
    }

    public function getContactNumbers(){
        $pdo = $this->getPdo();
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT %s FROM %s WHERE %s', 
                'contactInfo',
                'user_contacts',
                'userID = :userID AND ' . 
                'contactType = :contactType'
            )
        );
        $stmnt->bindValue(':contactType', 'P', PDO::PARAM_STR);
        $stmnt->bindValue(':userID', $this->userID, PDO::PARAM_INT);
        $stmnt->execute();
        return $stmnt->fetchAll(PDO::FETCH_OBJ);
    }

    public function getContactEmails(){
        $pdo = $this->getPdo();
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT %s FROM %s WHERE %s', 
                'contactInfo',
                'user_contacts',
                'userID = :userID AND ' . 
                'contactType = :contactType'
            )
        );
        $stmnt->bindValue(':contactType', 'E', PDO::PARAM_STR);
        $stmnt->bindValue(':userID', $this->userID, PDO::PARAM_INT);
        $stmnt->execute();
        return $stmnt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @param $value = value to search for in all model column's
     * @param $pdo
     * @return mixed
     */
    public static function findValue($value, $pdo)
    {
        $table = User::$tableName;
        $stmnt = $pdo->prepare(
            "SELECT * FROM $table WHERE username LIKE :value"
        );
        $stmnt->bindValue(":value", "%$value%");
        return parent::getModelsFromStatement('User', $stmnt, $pdo);
    }

}
