<?php
    include_once CLASS_DIR . 'Model.php';
    include_once MODEL_DIR . 'Notification.php';
    include_once MODEL_DIR . 'Skill.php';

    class Job extends Model{
        protected $jobID;
        protected $creatorID;
        protected $workerID;
        protected $fieldID;
        protected $cityID;
        protected $created;
        protected $title;
        protected $description;
        protected $status;

        public static $tableName = 'jobs';
        public static $privateAttributes = array();
        public static $publicAttributes = array(
                                                'title',
                                                'created',
                                                'jobID',
                                                'creatorID',
                                                'workerID',
                                                'cityID',
                                                'fieldID',
                                                'description',
                                                'points',
                                                'status',
                                            );

        public static $recordAttributes = array(
                                                'title',
                                                'creatorID',
                                                'fieldID',
                                                'cityID',
                                                'description',
                                            );

        public static $JOB_APP_HIRED = 'H';
        public static $JOB_APP_PENDING = 'P';
        public static $JOB_APP_FIRED = 'F';
        public static $JOB_APP_QUIT = 'Q';

        public static $JOB_COMPLETE = 'C';
        public static $JOB_PENDING = 'P';
        public static $JOB_AVAILABLE = 'A';

        /// @override
        protected function setupStatements(){
            $pdo = $this->getPdo();
            # update
            $query = sprintf(
                'UPDATE %s SET %s WHERE jobID = :jobID',
                Job::$tableName,
                'fieldID = :fieldID, description = :description, ' .
                'title = :title, workerID = :workerID, ' .
                'status = :status, cityID = :cityID, points = :points'
            );
            $updateStatement = $pdo->prepare($query);
            $updateStatement->bindParam(
                ':fieldID',
                $this->fieldID,
                PDO::PARAM_INT
            );
            $updateStatement->bindParam(
                ':description',
                $this->description, PDO::PARAM_STR
            );
            $updateStatement->bindParam(
                ':title',
                $this->title, PDO::PARAM_STR
            );
            $updateStatement->bindParam(
                ':jobID',
                $this->jobID,
                PDO::PARAM_INT
            );
            $updateStatement->bindParam(
                ':workerID',
                $this->workerID,
                PDO::PARAM_INT
            );
            $updateStatement->bindParam(
                ':cityID',
                $this->cityID,
                PDO::PARAM_INT
            );
            $updateStatement->bindParam(
                ':points',
                $this->points,
                PDO::PARAM_INT
            );
            $updateStatement->bindParam(
                ':status',
                $this->status,
                PDO::PARAM_STR
            );

            # delete
            $query = sprintf(
                'DELETE FROM %s WHERE jobID = :jobID',
                Job::$tableName
            );
            $deleteStatement = $pdo->prepare($query);
            $deleteStatement->bindParam(
                ':jobID',
                $this->jobID,
                PDO::PARAM_INT
            );
            $skillStatement= $pdo->prepare(
                sprintf(
                    'SELECT %s FROM %s WHERE %s',
                    'skill.skillID, skill.name, skill.fieldID',
                    'job_skills jskill INNER JOIN ' . Skill::$tableName . ' skill ' .
                    'ON skill.skillID = jskill.skillID ',
                    'jskill.jobID = :jobID'
                )
            );
            $skillStatement->bindParam(':jobID', $this->jobID, PDO::PARAM_INT);
            $this->updateStatement = $updateStatement;
            $this->deleteStatement = $deleteStatement;
            $this->getSkillsStatement = $skillStatement;
        }

        /// @override
        public function isModelDataValid(){
            if(!isSkillNameValid($this->title))
                return 'Invalid title';
            if(!isIDValid($this->fieldID))
                return 'Invalid fieldID';
            if(!isIDValid($this->workerID) && $this->workerID != null)
                return 'Invalid workerID';
            if(!isIDValid($this->cityID))
                return 'Invalid cityID';
            if(!isIDValid($this->points))
                return 'Invalid points';
            if(!isJobStatusValid($this->status))
                return 'Invalid status';
            if(strlen($this->description) < 10)
                return 'Invalid description';
            return parent::isModelDataValid();
        }

        /// @override
        protected static function insert($record, $pdo){
            $query = sprintf(
                'INSERT INTO %s (%s) VALUES (%s)',
                Job::$tableName,
                'creatorID, fieldID, cityID, title, description',
                ':creatorID, :fieldID, :cityID, :title, :description'
            );
            $stmnt = $pdo->prepare($query);
            $stmnt->bindValue(
                ':creatorID',
                $record['creatorID'],
                PDO::PARAM_INT
            );
            $stmnt->bindValue(
                ':fieldID',
                $record['fieldID'],
                PDO::PARAM_INT
            );
            $stmnt->bindValue(
                ':cityID',
                $record['cityID'],
                PDO::PARAM_INT
            );
            $stmnt->bindValue(
                ':title',
                ucwords($record['title']),
                PDO::PARAM_STR
            );
            $stmnt->bindValue(
                ':description',
                ucfirst($record['description']),
                PDO::PARAM_STR
            );
            $stmnt->execute();
        }

        /// @override
        protected static function isRecordValid($record, $pdo){
            if(!isSkillNameValid($record['title']))
                return 'Invalid title';
            if(!isIDValid($record['fieldID']))
                return 'Invalid fieldID';
            if(!isIDValid($record['cityID']))
                return 'Invalid cityID';
            if(!isIDValid($record['creatorID']))
                return 'Invalid creatorID';
            if(strlen($record['description']) < 10)
                return 'Invalid description';
            return parent::isRecordValid($record, $pdo);
        }

        /// @override
        public static function create($record, $pdo){
            parent::create_('Job', $record, $pdo);
            $id = $pdo->lastInsertId();
            return Job::getByJobID($id, $pdo);
        }

        /// @override
        public static function getAll($pdo){
            return parent::getAll_('Job', $pdo, "ORDER BY created DESC");
        }

        /***
         * Returns a new Model with matching jobID
         * @param $jobID = int id
         * @param $pdo = PDO connection to be used when executing queries
         * @return Job
         ***/
        public static function getByJobID($jobID, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE jobID = :jobID',
                    Job::$tableName
                )
            );
            $stmnt->bindValue(':jobID', $jobID, PDO::PARAM_INT);
            return parent::getModelFromStatement('Job', $stmnt, $pdo);
        }

        public function getApplicationState($userID){
            $pdo = $this->getPdo();
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT state FROM job_applications WHERE %s', 
                    'jobID = :jobID AND userID = :userID'
                )
            );
            $stmnt->bindValue(':jobID', $this->jobID, PDO::PARAM_INT);
            $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
            $stmnt->execute();
            $obj = $stmnt->fetch(PDO::FETCH_OBJ);
            if($obj != null)
                return $obj->state;
            return null;
        }

        public function apply($userID){
            if($userID == $this->creatorID) {
                throw new Xception(
                    "User forcefully applied for self created job",
                    Xception::$ERR_SUSPICIOUS
                );
            }
            if($this->status != $this::$JOB_AVAILABLE) {
                throw new Xception(
                    "User forcefully applied for closed created job",
                    Xception::$ERR_SUSPICIOUS
                );
            }
            $pdo = $this->getPdo();
            $stmnt = $pdo->prepare(
                sprintf(
                    'INSERT INTO job_applications (%s) VALUES (%s)', 
                    'userID, jobID',
                    ':userID, :jobID'
                )
            );
            $stmnt->bindValue(':jobID', $this->jobID, PDO::PARAM_INT);
            $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
            $stmnt->execute();
            $this->notify($this->creatorID, "New job applicant for " . $this->title);
        }

        protected function setApplicationState($userID, $state){
            $pdo = $this->getPdo();
            $stmnt = $pdo->prepare(
                sprintf(
                    'UPDATE job_applications SET %s WHERE userID = :userID AND jobID = :jobID',
                    'state = :state'
                )
            );
            $stmnt->bindValue(':jobID', $this->jobID, PDO::PARAM_INT);
            $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
            $stmnt->bindValue(':state', $state, PDO::PARAM_STR);
            $stmnt->execute();
        }

        protected function notify($userID, $message){
            $pdo = $this->getPdo();
            Notification::create(
                array('userID' => $userID, 'message'=>$message),
                $pdo
            );
        }

        public function hireWorker($userID){
            if($this->getApplicationState($userID) == $this::$JOB_APP_PENDING){
                $this->setApplicationState($userID, $this::$JOB_APP_HIRED);
                $this->notify($userID, 'You have been hired for ' . $this->title);
                $this->workerID = $userID;
                $this->status = $this::$JOB_PENDING;
                $this->save();
            } else{
                throw new Xception(
                    "Hire called on user without pending application",
                    Xception::$ERR_INVALID_DATA,
                    $userID
                );
            }
        }

        public function fireWorker($userID){
            if($this->getApplicationState($userID) == $this::$JOB_APP_HIRED){
                $this->setApplicationState($userID, $this::$JOB_APP_FIRED);
                $this->notify($userID, 'You have been fired from ' . $this->title);
                $this->workerID = null;
                $this->status = $this::$JOB_AVAILABLE;
                $this->save();
            } else{
                throw new Xception(
                    "Fire called on unhired user",
                    Xception::$ERR_INVALID_DATA,
                    $userID
                );
            }
        }

        public function quitJob($userID){
            if($this->getApplicationState($userID) == $this::$JOB_APP_HIRED){
                $this->setApplicationState($userID, $this::$JOB_APP_QUIT);
                $this->notify($this->creatorID, '... Has quit from ' . $this->title);
                $this->workerID = null;
                $this->status = $this::$JOB_AVAILABLE;
                $this->save();
            } else{
                throw new Xception(
                    "Quit called on unhired user",
                    Xception::$ERR_INVALID_DATA,
                    $userID
                );
            }
        }

        public static function getAllByCreatorID($userID, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE creatorID = :creatorID ORDER BY created DESC',
                    Job::$tableName
                )
            );
            $stmnt->bindValue(':creatorID', $userID, PDO::PARAM_INT);
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }

        public static function getAllByWorkerID($workerID, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE workerID = :workerID ORDER BY created DESC',
                    Job::$tableName
                )
            );
            $stmnt->bindValue(':workerID', $workerID, PDO::PARAM_INT);
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }

        public static function getAllAvailable($pdo){
            $stmnt = $pdo->query(
                sprintf(
                    'SELECT * FROM %s WHERE status = \'' . Job::$JOB_AVAILABLE. '\' ORDER BY created DESC',
                    Job::$tableName
                )
            );
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }

        public static function getAllByWorkerIDStatus($workerID, $status, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE workerID = :workerID AND status = :status ORDER BY created DESC',
                    Job::$tableName
                )
            );
            $stmnt->bindValue(':workerID', $workerID, PDO::PARAM_INT);
            $stmnt->bindValue(':status', $status, PDO::PARAM_STR);
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }

        public static function getAllByCreatorIDStatus($creatorID, $status, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE creatorID = :creatorID AND status = :status ORDER BY created DESC',
                    Job::$tableName
                )
            );
            $stmnt->bindValue(':creatorID', $creatorID, PDO::PARAM_INT);
            $stmnt->bindValue(':status', $status, PDO::PARAM_STR);
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }

        public static function getAllByUserIDApplicationState($userID, $state, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT j.* FROM %s WHERE userID = :userID AND ja.state = :state ORDER BY ja.created DESC',
                    Job::$tableName . ' j INNER JOIN job_applications ja ON j.jobID = ja.jobID'
                )
            );
            $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
            $stmnt->bindValue(':state', $state, PDO::PARAM_STR);
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }
        /**
         * @param $value = value to search for in all model column's
         * @param $pdo
         * @return mixed
         */
        public static function findValue($value, $pdo)
        {
            $table = Job::$tableName;
            $stmnt = $pdo->prepare(
                "SELECT * FROM $table WHERE title LIKE :value OR description LIKE :value"
            );
            $stmnt->bindValue(":value", "%$value%");
            return parent::getModelsFromStatement('Job', $stmnt, $pdo);
        }

    }
