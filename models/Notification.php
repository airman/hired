<?php

class Notification extends Model{
    
    protected $notificationID;
    protected $userID;
    protected $message;
    protected $unread;

    public static $tableName = 'user_notifications';
    public static $privateAttributes = array();
    public static $publicAttributes = array(
                                          'notificationID',  
                                          'userID',  
                                          'message',  
                                          'unread',
                                        );
    public static $recordAttributes = array(  
                                          'userID',  
                                          'message'  
                                        );

    public static $READ = false;
    public static $UNREAD = true;
    /**
     * Creates the update and insert PDO statement handlers
     * before hand and binds them to the corresponding
     * parameters.
     *
     * @return null
     **/
    protected function setupStatements()
    {
        $pdo = $this->getPdo();
        # update
        $query = sprintf(
            'UPDATE %s SET %s WHERE notificationID = :notificationID',
            Notification::$tableName,
            'message = :message'
        );
        $updateStatement = $pdo->prepare($query);
        $updateStatement->bindParam(
            ':message',
            $this->message,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':notificationID',
            $this->notificationID,
            PDO::PARAM_INT
        );
        # delete
        $query = sprintf(
            'DELETE FROM %s WHERE notificationID = :notificationID',
            Notification::$tableName
        );
        $deleteStatement = $pdo->prepare($query);
        $deleteStatement->bindParam(
            ':notificationID',
            $this->notificationID,
            PDO::PARAM_INT
        );
        $this->updateStatement = $updateStatement;
        $this->deleteStatement = $deleteStatement;        
    }

    public function isModelDataValid()
    {
        if(!($this->unread == true || $this->unread == false))
            return 'Invalid unread';
        return parent::isModelDataValid(); 
    }


    /**
     * Inserts new record into Model table
     *
     * @param $record = array containing key value store of
     *                        column messages and the value to be inserted
     * @param $pdo = PDO connection to be used when executing queries
     *
     * @return null
     **/
    protected static function insert($record, $pdo)
    {
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            Notification::$tableName,
            'message, userID',
            ':message, :userID'
        );
        $stmnt = $pdo->prepare($query);
        $stmnt->bindValue(
            ':message',
            $record['message'],
            PDO::PARAM_STR
        );
        $stmnt->bindValue(
            ':userID',
            $record['userID'],
            PDO::PARAM_INT
        );
        $stmnt->execute();
    }

    /// @override
    protected static function isRecordValid($record, $pdo){
        if(!isIDValid($record['userID']))
            return 'Invalid userID';
        return parent::isRecordValid($record, $pdo);
    }

    /**
     * Inserts new record into Model table and returns a new Model
     * object
     * @param $record = array containing key value store of
     *                        column messages and the value to be inserted
     * @param $pdo = PDO connection to be used when executing queries
     * @return Model
     **/
    public static function create($record, $pdo)
    {
        parent::create_('Notification', $record, $pdo);
        $id = $pdo->lastInsertId();
        return Notification::getByNotificationID($id, $pdo);
    }

    /**
     * Returns an array of Model objects for all records
     * in Model table, wraps getAll_() function
     *
     * @param $pdo = PDO connection to be used when executing queries
     *
     * @return array
     **/
    /// @override
    public static function getAll($pdo){
        return parent::getAll_('Notification', $pdo, "ORDER BY created DESC");
    }

    public static function getByNotificationID($notificationID, $pdo)
    {
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE notificationID = :notificationID',
                Notification::$tableName
            )
        );
        $stmnt->bindValue(':notificationID', $notificationID, PDO::PARAM_INT);
        return parent::getModelFromStatement('Notification', $stmnt, $pdo);
    }

    public static function getAllByUserID($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE userID = :userID ORDER BY created DESC',
                Notification::$tableName
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        return parent::getModelsFromStatement('Notification', $stmnt, $pdo);
    }

    public static function getAllByUserIDUnread($userID, $unread, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE userID = :userID AND unread = :unread ORDER BY created DESC',
                Notification::$tableName
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        $stmnt->bindValue(':unread', $unread, PDO::PARAM_INT);
        return parent::getModelsFromStatement('Notification', $stmnt, $pdo);
    }

    public static function getUserNotificationCount($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT count(userID) as count FROM %s WHERE userID = :userID',
                Notification::$tableName
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        $stmnt->execute();
        return $stmnt->fetch(PDO::FETCH_OBJ)->count;
    }

    public static function getUnreadUserNotificationCount($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT count(userID) as count FROM %s WHERE userID = :userID AND unread = TRUE',
                Notification::$tableName
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        $stmnt->execute();
        return $stmnt->fetch(PDO::FETCH_OBJ)->count;
    }

    public static function markAllRead($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'UPDATE %s SET %s WHERE userID = :userID',
                Notification::$tableName,
                'unread = FALSE'
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
        $stmnt->execute();
    }

    /**
     * @param $value = value to search for in all model column's
     * @param $pdo
     * @return mixed
     */
    public static function findValue($value, $pdo)
    {
        $table = Notification::$tableName;
        $stmnt = $pdo->prepare(
            "SELECT * FROM $table WHERE message LIKE :value"
        );
        $stmnt->bindValue(":value", "%$value%");
        return parent::getModelsFromStatement('Notification', $stmnt, $pdo);
    }

} 