<?php

include_once CLASS_DIR . 'Model.php';

class Field extends Model{

    protected $name;
    protected $fieldID;


    public static $tableName = 'fields';
    public static $privateAttributes = array();
    public static $publicAttributes = array(
        'name',
        'fieldID'
    );
    public static $recordAttributes = array(
        'name'
    );

    /// @override
    protected function setupStatements(){
        $pdo = $this->getPdo();
        # update
        $query = sprintf(
            'UPDATE %s SET %s WHERE fieldID = :fieldID',
            Field::$tableName,
            'name = :name'
        );
        $updateStatement = $pdo->prepare($query);
        $updateStatement->bindParam(
            ':name',
            $this->name,
            PDO::PARAM_STR
        );
        $updateStatement->bindParam(
            ':fieldID',
            $this->fieldID,
            PDO::PARAM_INT
        );

        # delete
        $query = sprintf(
            'DELETE FROM %s WHERE fieldID = :fieldID',
            Field::$tableName
        );
        $deleteStatement = $pdo->prepare($query);
        $deleteStatement->bindParam(
            ':fieldID',
            $this->fieldID,
            PDO::PARAM_INT
        );
        $this->updateStatement = $updateStatement;
        $this->deleteStatement = $deleteStatement;
    }

    /// @override
    public function isModelDataValid(){
        if(!isSkillNameValid($this->name))
            return 'Invalid field name';
        if(!isIDValid($this->fieldID))
            return 'Invalid fieldID';
        return parent::isModelDataValid();
    }

    /// @override
    protected static function insert($record, $pdo){
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            Field::$tableName,
            'name',
            ':name'
        );
        $stmnt = $pdo->prepare($query);
        $stmnt->bindValue(
            ':name',
            ucwords($record['name']),
            PDO::PARAM_STR
        );
        $stmnt->execute();
    }

    /// @override
    protected static function isRecordValid($record, $pdo){
        if(!isSkillNameValid($record['name']))
            return 'Invalid field name';
        return parent::isRecordValid($record, $pdo);
    }

    /// @override
    public static function create($record, $pdo){
        parent::create_('Field', $record, $pdo);
        $id = $pdo->lastInsertId();
        return Field::getByFieldID($id, $pdo);
    }

    /// @override
    public static function getAll($pdo){
        return parent::getAll_('Field', $pdo);
    }

    /***
     * Returns a new Model with matching fieldID
     * @param $fieldID = int id
     * @param $pdo = PDO connection to be used when executing queries
     * @return Field
     ***/
    public static function getByFieldID($fieldID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE fieldID = :fieldID',
                Field::$tableName
            )
        );
        $stmnt->bindValue(':fieldID', $fieldID, PDO::PARAM_INT);
        return parent::getModelFromStatement('Field', $stmnt, $pdo);
    }

    public static function getByName($fieldName, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT * FROM %s WHERE name = :fieldName',
                Field::$tableName
            )
        );
        $stmnt->bindValue(':fieldName', $fieldName, PDO::PARAM_STR);
        return parent::getModelFromStatement('Field', $stmnt, $pdo);
    }

    public static function getAllByUserID($userID, $pdo){
        $stmnt = $pdo->prepare(
            sprintf(
                'SELECT f.fieldID, f.name FROM %s WHERE us.userID = :userID',
                Field::$tableName . ' f INNER JOIN skills s ON s.fieldID = f.fieldID '.
                'INNER JOIN user_skills us ON us.skillID = s.skillID'
            )
        );
        $stmnt->bindValue(':userID', $userID, PDO::PARAM_STR);
        return parent::getModelsFromStatement('Field', $stmnt, $pdo);
    }

    /**
     * @param $value = value to search for in all model column's
     * @param $pdo
     * @return mixed
     */
    public static function findValue($value, $pdo)
    {
        $table = Field::$tableName;
        $stmnt = $pdo->prepare(
            "SELECT * FROM $table WHERE name LIKE :value"
        );
        $stmnt->bindValue(":value", "%$value%");
        return parent::getModelsFromStatement('Field', $stmnt, $pdo);
    }

}