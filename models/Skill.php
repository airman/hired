<?php

    include_once CLASS_DIR . 'Model.php';


    class Skill extends Model{

        protected $skillID;
        protected $name;
        protected $fieldID;

        public static $tableName = 'skills';
        public static $privateAttributes = array();
        public static $publicAttributes = array(
                                                'skillID',
                                                'name',
                                                'fieldID'
                                            );
        public static $recordAttributes = array(
                                                'name',
                                                'fieldID'
                                            );

        /// @override
        protected function setupStatements(){
            $pdo = $this->getPdo();
            # update
            $query = sprintf(
                'UPDATE %s SET %s WHERE skillID = :skillID',
                Skill::$tableName,
                'name = :name, fieldID = :fieldID'
            );
            $updateStatement = $pdo->prepare($query);
            $updateStatement->bindParam(
                ':name',
                $this->name,
                PDO::PARAM_STR
            );
            $updateStatement->bindParam(
                ':fieldID',
                $this->fieldID,
                PDO::PARAM_STR
            );
            $updateStatement->bindParam(
                ':skillID',
                $this->skillID,
                PDO::PARAM_INT
            );

            # delete
            $query = sprintf(
                'DELETE FROM %s WHERE skillID = :skillID',
                Skill::$tableName
            );
            $deleteStatement = $pdo->prepare($query);
            $deleteStatement->bindParam(
                ':skillID',
                $this->skillID,
                PDO::PARAM_INT
            );
            $this->updateStatement = $updateStatement;
            $this->deleteStatement = $deleteStatement;
        }

        /// @override
        public function isModelDataValid(){
            if(!isSkillNameValid($this->name))
                return 'Invalid skill name';
            if(!isIDValid($this->fieldID))
                return 'Invalid fieldID';
            return parent::isModelDataValid();
        }

        /// @override
        protected static function insert($record, $pdo){
            $query = sprintf(
                'INSERT INTO %s (%s) VALUES (%s)',
                Skill::$tableName,
                'name, fieldID',
                ':name, :fieldID'
            );
            $stmnt = $pdo->prepare($query); 
            $stmnt->bindValue(
                ':name', 
                ucwords($record['name']),
                PDO::PARAM_STR
            );
            $stmnt->bindValue(
                ':fieldID', 
                $record['fieldID'],
                PDO::PARAM_STR
            );
            $stmnt->execute();
        }

        /// @override
        protected static function isRecordValid($record, $pdo){
            if(!isSkillNameValid($record['name']))
                return 'Invalid skill name';
            if(!isIDValid($record['fieldID']))
                return 'Invalid fieldID';
            return parent::isRecordValid($record, $pdo);
        }

        /// @override
        public static function create($record, $pdo){
            parent::create_('Skill', $record, $pdo);
            $id = $pdo->lastInsertId();
            return Skill::getBySkillID($id, $pdo);
        }

        /// @override
        public static function getAll($pdo){
            return parent::getAll_('Skill', $pdo);
        }

        /***
         * Returns a new Model with matching skillID
         * @param $skillID = int id
         * @param $pdo = PDO connection to be used when executing queries
         * @return Skill 
         ***/
        public static function getBySkillID($skillID, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE skillID = :skillID',
                    Skill::$tableName
                )
            );
            $stmnt->bindValue(':skillID', $skillID, PDO::PARAM_INT);
            return parent::getModelFromStatement('Skill', $stmnt, $pdo);
        }

        public static function getByName($skillName, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT * FROM %s WHERE name = :skillName',
                    Skill::$tableName
                )
            );
            $stmnt->bindValue(':skillName', $skillName, PDO::PARAM_STR);
            return parent::getModelFromStatement('Skill', $stmnt, $pdo);
        }

        public static function getAllByUserID($userID, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT s.skillID, s.name, s.fieldID FROM %s WHERE us.userID = :userID',
                    Skill::$tableName . ' s INNER JOIN user_skills us ON ' .
                    's.skillID = us.skillID'
                )
            );
            $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
            return parent::getModelsFromStatement('Skill', $stmnt, $pdo);
        }

        public static function getAllByJobID($jobID, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT s.skillID, s.name, s.fieldID FROM %s WHERE js.jobID = :jobID',
                    Skill::$tableName . ' s INNER JOIN job_skills js ON ' .
                    's.skillID = js.skillID'
                )
            );
            $stmnt->bindValue(':jobID', $jobID, PDO::PARAM_INT);
            return parent::getModelsFromStatement('Skill', $stmnt, $pdo);
        }

        public static function getAllByUserIDFieldName($userID, $fieldName, $pdo){
            $stmnt = $pdo->prepare(
                sprintf(
                    'SELECT s.skillID, s.name, s.fieldID FROM %s WHERE us.userID = :userID AND f.name = :fieldName',
                    Skill::$tableName . ' s INNER JOIN user_skills us ON ' .
                    's.skillID = us.skillID INNER JOIN fields f ON ' .
                    's.fieldID = f.fieldID'
                )
            );
            $stmnt->bindValue(':userID', $userID, PDO::PARAM_INT);
            $stmnt->bindValue(':fieldName', $fieldName, PDO::PARAM_STR);
            return parent::getModelsFromStatement('Skill', $stmnt, $pdo);
        }

        public static function setJobSkills($jobID, $skillNames, $pdo){
            $pdo->exec("DELETE FROM job_skills WHERE jobID = " . $jobID);
            $skillID = -1;
            $stmnt = $pdo->prepare(
                sprintf('INSERT INTO %s (%s) VALUES (%s)',
                    'job_skills',
                    'skillID, jobID',
                    ':skillID, :jobID'
                )
            );
            $stmnt->bindParam(":jobID", $jobID, PDO::PARAM_INT);
            $stmnt->bindParam(":skillID", $skillID, PDO::PARAM_INT);
            foreach($skillNames as $skillName){
                $skill = Skill::getByName($skillName, $pdo);
                if($skill == null){
                    throw new Xception(
                        "Received Unknown skill name",
                        Xception::$ERR_INVALID_DATA,
                        $skillName
                    );
                }
                $skillID = $skill->getSkillID();
                $stmnt->execute();
            }
        }

        public static function setUserSkills($userID, $skillNames, $pdo){
            $pdo->exec("DELETE FROM user_skills WHERE userID = " . $userID);
            $skillID = -1;
            $stmnt = $pdo->prepare(
                sprintf('INSERT INTO %s (%s) VALUES (%s)',
                    'user_skills',
                    'skillID, userID',
                    ':skillID, :userID'
                )
            );
            $stmnt->bindParam(":userID", $userID, PDO::PARAM_INT);
            $stmnt->bindParam(":skillID", $skillID, PDO::PARAM_INT);
            foreach($skillNames as $skillName){
                $skill = Skill::getByName($skillName, $pdo);
                if($skill == null){
                    throw new Xception(
                        "Received Unknown skill name",
                        Xception::$ERR_INVALID_DATA,
                        $skillName
                    );
                }

                $skillID = $skill->getSkillID();
                $stmnt->execute();
            }
        }

        /**
         * @param $value = value to search for in all model column's
         * @param $pdo
         * @return mixed
         */
        public static function findValue($value, $pdo)
        {
            $table = Skill::$tableName;
            $stmnt = $pdo->prepare(
                "SELECT * FROM $table WHERE name LIKE :value"
            );
            $stmnt->bindValue(":value", "%$value%");
            return parent::getModelsFromStatement('Skill', $stmnt, $pdo);
        }


    }
