
Setup Site:
    
    1.  Run create_tables.sql using your preferred sql database manager
    2.  Edit config.php     
            - set the db properties for your database, i.e
                home_url, host, user, password, database name and the
                database manager in dsn variable, default 'mysql'
                
                dsn variable is used when creating database connection
                using Php PDO objects.

                preferably 'msql' as I haven't tested it with others 

    3. Run your server and everything should work fine

Run tests:
    
    Linux:
        Dependancies:
            - php5
            - phpunit
            - phpdoc
            - phploc
            - phpcpd
            
        Edit tests/prepareDB.sh to use the correct database username and
        password.

        This requires apache-ant to be installed in order to avoid
        doing it manually.

         
        Using Apache-ant:
            Simply type 'ant full-build' in your terminal to run test for the
            first time, this is to setup test database as well. 
        
            After the first run, you can simply run 'ant'
        
    
