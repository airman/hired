<?php
require_once 'config.php';
require_once HELPER_DIR . 'validation.php';
require_once HELPER_DIR . 'utils.php';
require_once CLASS_DIR . 'Xception.php';
require_once CLASS_DIR . 'Acl.php';
require_once CLASS_DIR . 'View.php';

session_start();

$acl = new Acl();
$view = new View();

$ALERT_INFO = 'alert-info';
$ALERT_DANGER = 'alert-danger';
$ALERT_WARNING = 'alert-warning';
$ALERT_SUCCESS = 'alert-success';

$loggedIn = isLoggedIn();
$default_page = ($loggedIn)? 'home':'signup';

$page = $default_page;
if(isset($_GET['page'])){
    $page = $_GET['page'];
}
if(!$acl->pageExists($page)){
    $page = "error";
}elseif(!$acl->hasAccessToPage($page)){
    $page = $default_page;
    redirectedAlert(
        "?page=$default_page",
        "You are not authorised to access that feature.",
        $ALERT_DANGER
    );
    return;
}


if($loggedIn){
    require_once MODEL_DIR . 'User.php';
    require_once MODEL_DIR . 'Profile.php';
    require_once MODEL_DIR . "Notification.php";

    $userID =  $_SESSION['user']['id'];
    $pdo = newPDO();
    $user = User::getByUserID($userID,
        $pdo
    );
    $profile = Profile::getByUserID($userID, $pdo);
    $notifCount = Notification::getUnreadUserNotificationCount($userID, $pdo);

    $actions = array();
    $actions['home'] =
        ($page == 'home')? 'active': '';
    $actions['profile'] =
        ($page == 'profile')? 'active': '';
    $actions['notifications'] =
        ($page == 'notifications')? 'active': '';
    $actions['connections'] =
        ($page == 'connections')? 'active': '';
}

# TODO set this up to display appropriate message to user
# so as not to leak critical information
try{
    require_once VIEW_DIR . $page . '.php';
}catch(Xception $e){
    echo $e->getMessage() . PHP_EOL;
    var_dump($e->getData());
}catch(Exception $e){
    echo $e->getMessage() . PHP_EOL;
}



