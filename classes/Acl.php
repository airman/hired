<?php
    /***
     * Controls access to views
     ***/

    class Acl{
        private $role;
        private $acl;
        private $roles = array(
                            0 => 'guest',
                            1 => 'user',
                            9 => 'admin'
                        );
        public $admin = array(
                            'admin',
                            'users',
                            'createUser',
                            'deleteUser',
                        );
        public $guest = array(
                            'register',
                            'signup', 
                            'login',
                            'error'
                        );
        public $user = array(
                            'logout',
                            'home',
                            'profile',
                            'connections',
                            'notifications',
                            'updateProfile',
                            'createJob',
                            'applyForJob',
                            'job',
                            'settings',
                            'search'
                        );

        public function __construct(){
            $this->acl['guest'] = $this->guest;
            $this->acl['user'] = array_merge($this->guest, $this->user);
            $this->acl['admin'] = array_merge($this->user, $this->admin);
        }

        /***
         * Return true if current access level is assigned to page
         * @param $page = string page to be checked
         * @return boolean
         ***/
        public function hasAccessToPage($page){
            $role = $this->getRole();
            return in_array($page, $this->acl[$role]);
        }

        /***
         * Returns the current access level
         * @return integer
         ***/
        public function getAccessLevel(){
            return isset($_SESSION['user']['gid'])? $_SESSION['user']['gid']: 0;
        }

        /***
         * Returns the role of current access level
         * @return string
         ***/
        public function getRole(){
            if(!$this->role){
                $this->role = $this->roles[$this->getAccessLevel()];
            }
            return $this->role;
        }

        /**
         * @param $page string page to be checked
         * @return bool
         */
        public function pageExists($page){
            foreach($this->roles as $role){
                if(in_array($page, $this->acl[$role]))
                    return true;
            }
            return false;
        }
    }
