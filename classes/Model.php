<?php
/**
 * Mediates database interactions
 **/

require_once HELPER_DIR . 'validation.php';
require_once CLASS_DIR . 'Xception.php';

/**
 * The Model class represents an object that
 * will be used to mediate database interactions
 **/
abstract class Model{

    private $pdo;
    private $className;
    protected $updateStatement;
    protected $deleteStatement;

    /**
     * Concrete model objects should not ever be
     * instantiated using the constructor, it is public
     * so it can be used by the fetch method of the
     * pdo connection. This may change in further revisions
     * of this class.
     *
     * To instantiate new models from existing records in the database,
     * use the static methods provided, otherwise use the static method,
     * Model::create($record, $pdo) to instantiate
     * a new model and add the record to the database simultaneously.
     *
     * Should a model be instantiated using the constructor, there
     * may be complications regarding the retrieval and storage of
     * data from the database using the model object and the instantiated
     * object may be of little use.
     *
     * @param $pdo = PDO connection
     */
    public function __construct($pdo){
        $this->pdo = $pdo;
        $this->className = get_class($this);
        $this->setupStatements();
        $this->createAttributeMutators();
    }

    /**
     * Dynamic function server, calls dynamically created
     * methods
     *
     * @param $method = string name of method to be called
     * @param $args = array argument array
     *
     * @throws Exception
     *
     * @return null
     **/
    public function __call($method, $args)
    {
        $className = get_class( $this );
        $args = array_merge( array( $className => $this ), $args );

        if(isset($this->{$method}) && is_callable($this->{$method})){
            return call_user_func($this->{$method}, $args);
        } else {
            throw new Exception(
                "Fatal error: Call to undefined method $className::{$method}()"
            );
        }
    }

    /**
     * Returns pdo connection
     *
     * @return PDO
     **/
    public function getPdo()
    {
        return $this->pdo;
    }

    /**
     * Returns a json object string of all the
     * public attributes of the model
     *
     * @return string
     **/
    public function toJSON()
    {
        return json_encode($this::getJSONData());
    }

    /**
     * Returns an associative array containing the
     * data to be encoded into the JSON string in
     * Model::toJSON() this data is comprised of
     * the public attributes stored in static array
     * Model::$publicAttributes. Sensitive attributes
     * of the model should be placed in the
     * Model::$privateAttributes static array.
     *
     * @return array
     **/
    public function getJSONData()
    {
        $data = array();
        $cn = $this->className;
        foreach($cn::$publicAttributes as $attr){
            $data[$attr] = $this->{$attr};
        }
        return $data;
    }

    /**
     * Updates the model's corresponding db record
     * using the model's current attribute values
     *
     * @throws Xception
     *
     * @return null
     */
    public function save()
    {
        try{
            $valid = $this->isModelDataValid();
            if($valid == null){
                $this->updateStatement->execute();
            }else{
                throw new Xception(
                    $valid,
                    Xception::$ERR_INVALID_DATA
                );
            }
        }catch (PDOException $e){
            throw new Xception(
                $e->getMessage(),
                Xception::$ERR_INVALID_DATA
            );
        }
    }

    /**
     * Deletes corresponding db record
     * Once this is run, the object becomes useless
     * as most of the methods rely on the
     * corresponding db record as a result, this
     * function also sets the object attributes to null.
     *
     * @return null
     **/
    public function delete()
    {
        $this->deleteStatement->execute();
        $this->pdo = null;
        $cn = get_class($this);
        $attributes = array_merge(
            $cn::$privateAttributes,
            $cn::$publicAttributes
        );
        foreach($attributes as $attr)
            $this->$attr = null;
    }

    /**
     * Checks to see if model attributes have valid values,
     * as there is no validation within the dynamically
     * generated setters. This is to prevent the execution
     * of sql queries with invalid values during Model::save()
     * and Model::delete() calls or any other methods that
     * modify database data. The structure of this method is
     * similar to Model::isRecordValid().
     *
     * @return string|null
     **/
    public function isModelDataValid()
    {
        return null;
    }

    /**
     * Creates the update and insert PDO statement handlers
     * before hand and binds them to the corresponding
     * parameters.
     *
     * @return null
     **/
    protected abstract function setupStatements();

    /**
     *  Create setters and getters for Model's
     *  private and public attributes
     *
     * @return null
     **/
    private function createAttributeMutators()
    {
        $cn = get_class($this);
        $attributes = array_merge(
            $cn::$publicAttributes,
            $cn::$privateAttributes
        );
        foreach($attributes as $attr){
            $getter = 'get' . ucfirst($attr);
            $setter = 'set' . ucfirst($attr);
            $this->{$getter} = function () use ($attr){
                                return $this->$attr;
                             };
            $this->{$setter} = function ($args) use ($attr){
                                return $this->$attr = $args[0];
                             };
        }
    }

    /****
     *
     * STATIC methods
     *
     *****/

    /**
     * Returns a Model object of the given child class resulting from
     * executing the given statement
     *
     * @param $className = string name of concrete class inheriting
     *                            from Model
     * @param $stmnt = PDO statement to be executed
     * @param $pdo = PDO connection to be passed to Model objects
     *
     * @throws Xception
     *
     * @return Model
     **/
    protected static function getModelFromStatement($className, $stmnt, $pdo)
    {
        $stmnt->setFetchMode(
            PDO::FETCH_CLASS, 
            $className,
            array($pdo)
        );
        try{
            $stmnt->execute();
            return $stmnt->fetch();
        }catch(PDOException $e){
            throw new Xception(
                $e->getMessage(),
                Xception::$ERR_QUERY_FAILED
            );
        }
    }

    /**
     * Returns an array of Model objects of the given child class
     * resulting from executing the given statement
     *
     * @param $className = string name of concrete class inheriting
     *                     from Model
     * @param $stmnt = PDO statement to be executed
     * @param $pdo = PDO connection to be passed to Model objects
     *
     * @throws Xception
     *
     * @return array
     **/
    public static function getModelsFromStatement($className, $stmnt, $pdo)
    {
        $stmnt->setFetchMode(
            PDO::FETCH_CLASS, 
            $className, 
            array($pdo)
        );
        try{
            $stmnt->execute();
            return $stmnt->fetchAll();
        }catch(PDOException $e){
            throw new Xception(
                $e->getMessage(),
                Xception::$ERR_QUERY_FAILED
            );
        }
    }

    /**
     * Returns all records in table as array of Model objects
     *
     * @param $className = string name of concrete class inheriting
     *                            from Model
     * @param $pdo = PDO connection to be used when executing queries
     *
     * @param string $order specify order if necessary
     * @return array
     */
    public static function getAll_($className, $pdo, $order="")
    {
        $tableName = $className::$tableName;
        $stmnt = $pdo->query("SELECT * FROM $tableName $order");
        return $className::getModelsFromStatement($className, $stmnt, $pdo);
    } 

    /**
     * Validates the record data before attempting to insert
     * it into the database.
     *
     * @param $record = array containing key value store of
     *                        column names and the value to be inserted
     * @param $pdo = PDO connection to be used when executing queries
     *
     * @return boolean
     **/
    protected static function isRecordValid($record, $pdo)
    {
        return null;
    }

    /**
     * Validates and then inserts new record into Model table.
     *
     * @param $className = string name of concrete class inheriting
     *                            from Model
     * @param $record = array containing key value store of
     *                        column names and the value to be inserted
     * @param $pdo = PDO connection to be used when executing queries
     *
     * @throws Xception
     *
     * @return null
     **/
    protected static function create_($className, $record, $pdo)
    {
        $keys = $className::$recordAttributes;
        if(!arrayKeysSet($keys, $record)){
            throw new Xception(
                "Record for $className creation missing 1 or more keys",
                Xception::$ERR_MISSING_DATA,
                array_merge($keys, $record)
            );
        }
        $validation = $className::isRecordValid($record, $pdo);
        if($validation){
            throw new Xception(
                $validation,
                Xception::$ERR_INVALID_DATA,
                array_merge($keys, $record)
            );
        }
        try{
            $className::insert($record, $pdo);
            return true;
        }catch(PDOException $e){
            //echo "Model::create_ $className insert failed: ".
                //$e->getMessage() . PHP_EOL;
            
            throw new Xception(
                $e->getMessage(),
                Xception::$ERR_QUERY_FAILED
            );
        }
    }

    /**
     * Inserts new record into Model table
     *
     * @param $record = array containing key value store of
     *                        column names and the value to be inserted
     * @param $pdo = PDO connection to be used when executing queries
     *
     * @return null
     **/
    protected static abstract function insert($record, $pdo);

    /**
     * Inserts new record into Model table and returns a new Model
     * object
     * @param $record = array containing key value store of
     *                        column names and the value to be inserted
     * @param $pdo = PDO connection to be used when executing queries
     * @return Model
     **/
    public static abstract function create($record, $pdo);

    /**
     * Returns an array of Model objects for all records
     * in Model table, wraps getAll_() function
     *
     * @param $pdo = PDO connection to be used when executing queries 
     *
     * @return array
     **/
    public static abstract function getAll($pdo);

    /**
     * @param $value = value to search for in all model column's
     * @param $pdo
     * @return mixed
     */
    public static abstract function findValue($value, $pdo);
}
