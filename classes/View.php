<?php
    class View{
        private $attributes = array(
                            'title' => 'Part Timer',
                        );
        private $css = array();
        private $js = array();
        private $page = null;

        public function setPage($page){
            $this->page = $page;
        }

        public function getPage(){
            return $this->page;
        }

        public function setAttribute($attr, $value){
            $this->attributes[$attr] = $value;
        }

        public function getAttribute($attr){
            if(isset($this->attributes[$attr])){
                return $this->attributes[$attr];
            }
            return null;
        }

        public function addCSS($css){
            array_push($this->css, $css);
        }

        public function includeCSS(){
            $html = "";
            //echo "includeCSS: ". ASSET_DIR . PHP_EOL;
            foreach($this->css as $css){
                $html .= sprintf(
                            '<link rel="stylesheet" href="%s"/>' . PHP_EOL,
                            HOME_URL . 'assets/css/' . $css
                        );
            }
            echo $html;
        }

        public function addJS($js){
            array_push($this->js, $js);
        }

        public function includeJS(){
            $html = "";
            //echo "includeJS: ". ASSET_DIR . PHP_EOL;
            foreach($this->js as $js){
                $html .= sprintf(
                            '<script type="text/javascript" src="%s"></script>' . PHP_EOL,
                            HOME_URL . 'assets/js/' . $js
                        );
            }
            echo $html;
        }
    }