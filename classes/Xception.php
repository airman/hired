<?php
class Xception extends Exception{
    public static $ERR_INVALID_DATA  =  10;
    public static $ERR_MISSING_DATA  =  11;
    public static $ERR_INVALID_TOKEN =  12;
    public static $ERR_QUERY_FAILED  =  13;
    public static $ERR_NOT_LOGGEDIN  =  20;
    public static $ERR_UNAUTHORISED  =  21;
    public static $ERR_UNAVAILABLE   =  22;
    public static $ERR_404           =  30;
    public static $ERR_SUSPICIOUS    =  31;
    public static $ERR_UNEXPECTED    =  32;

    public $validErrorCodes = array(
                                10, 11, 12, 13, 20, 
                                21, 22, 30, 31, 32
                            );
    public $userMessages = array(
                                10 => "Invalid request sent",
                                11 => "Incomplete request sent",
                                12 => "Stale request submitted, please try again",
                                13 => "Invalid request sent",
                                20 => "Please log in to continue",
                                21 => "You are not authorised to access that feature",
                                22 => "That feature is currently unavailable",
                                30 => "Page not found",
                                31 => "We have detected suspicious behaviour from you, your actions have been logged.",
                                32 => "Sorry, something unexpected occured, please try again"
                            );

    public $data;

    public function __construct($msg, $code, $data=null){
        assert(in_array($code, $this->validErrorCodes));
        parent::__construct($msg, $code);
        $this->data = $data;
    }

    public function getUserMessage(){
        return $this->userMessages[$this->getCode()];
    }

    public function getData(){
        return $this->data;
    }


} 
