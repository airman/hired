<?php
$resultCount = count($jobs);
$resultPageCount = 1;
$jobCount = count($jobs);
$end = ($jobCount > $resultSize - $start)? $resultSize:$jobCount;
if($resultCount > $resultSize)
    $resultPageCount = ceil($resultCount/(float)$resultPage);

for($i =$start; $i < $end; $i++){
    $job = $jobs[$i];
    $jobID = $job->getJobID();
    $jobSkills = Skill::getAllByJobID($jobID, $pdo);
    $jobCity = City::getByCityID($job->getCityID(), $pdo);
    $jobField = Field::getByFieldID($job->getFieldID(), $pdo);
    $authorProfile = Profile::getByUserID($job->getCreatorID(), $pdo);

    $jobTitle = $job->getTitle();
    $jobCityName = $jobCity->getName();
    $authorName = $authorProfile->getFullName();
    $authorURL = "#";
    $jobDescription = $job->getDescription();
    $jobAge = getAge($job->getCreated(), $date);
    $jobInfo = "$jobAge by <a href='$authorURL'>$authorName</a>";

    $fieldIcons = array(
        "FieldA" => "glyphicon-wrench",
        "FieldB" => "glyphicon-wrench",
        "FieldC" => "glyphicon-wrench"
    );
    $jobIcon = $fieldIcons[$jobField->getName()];
    ?>

    <div class="feed-item job" id="job-<?php echo $jobID;?>">
        <div class="job-icon hidden-xs">
            <span class="glyphicon <?php echo $jobIcon?>"></span>
        </div>
        <div class="job-details">
            <span class="job-title"><a href="#"><?php echo $jobTitle;?></a></span>
            <ul class="job-skills">
                <?php
                    foreach($jobSkills as $skill) echo "<li>" . $skill->getName() . "</li>\n";
                ?>
            </ul>
		<span class="job-description">
            <p><b><a href="#"><?php echo $jobCityName;?></a></b></p>
            <?php echo $jobDescription;?>
		</span>
            <span class="job-post-info"><?php echo $jobInfo;?></span>
        </div>
        <ul class="job-actions">
            <li class="main-action" onclick="applyForJob(<?php echo $jobID;?>)">
                <span class="glyphicon glyphicon-ok"></span>
            </li>
            <li class="sub-actions" onclick="toggleJob(<?php echo $jobID;?>)" >
                <ul>
                    <li class="toggleDescription"><span class="fa-angle-double-down"></span></li>
                </ul>
            </li>
        </ul>
    </div>
<?php
}
?>
<?php
if($jobCount < 1){
?>
    <div class="feed-item job feed-item-empty">
        <div class="job-details">
            <br/>
            <span class="job-title"></span>
            <br/>
        </div>
    </div>

<?php
}
?>
