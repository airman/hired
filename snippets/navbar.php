
<nav id="autocollapse" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div id="nav-items" class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <ul class="nav navbar-nav user-actions" style="margin-left: 10px;">
                <li class="pull-left <?php echo $actions['home'];?>">
                    <a href="?page=home"><span style="font-size:1.5em;" class="glyphicon glyphicon-home"></a>
                </li>
                <li class="pull-left <?php echo $actions['connections'];?>">
                    <a href="?page=connections"><span style="font-size:1.5em;" class="fa-group"></a>
                </li>
                <li class="pull-left <?php echo $actions['notifications'];?>">
                    <?php

                    if($notifCount > 0 && $page != 'notifications') {
                        ?>
                        <a href="?page=notifications"><span style="font-size:1.5em;color:#788ff2;" class="fa-envelope"></span></a>
                        <?php

                        echo "<span id=\"notif-count\">$notifCount</span>" . PHP_EOL;
                    }
                    else{
                        ?>
                        <a href="?page=notifications"><span style="font-size:1.5em;" class="fa-envelope"></span></a>
                    <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <form id="formSearch" class="navbar-form navbar-nav" role="search" method="POST" action="?page=search">
                <div class="input-group">
                    <input class="form-control" type="text" name="search" placeholder="Search for jobs or users">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
            </form>
            <ul class="nav navbar-nav">
                <a href="?page=profile">
                <li class="your-avatar">
                        <img src="<?php echo ASSETS . "img/" . $profile->getAvatar();?>" alt="user-avatar">
                        <span><?php echo $profile->getPoints();?></span>
                </li>
                </a>
                <li class="dropdown">
                    <a style="text-shadow:none;" class="action-link" href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <b>Account</b>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="?page=settings" class="action-link"><span class="fa-cog"></span> Settings</a>
                            <a href="?page=logout" class="action-link"><span class="fa-power-off"> </span>Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<script>
    function autocollapse() {
        var navbar = $('#autocollapse');
        navbar.removeClass('collapsed'); // set standard view
        if(navbar.innerHeight() > 60){ // check if we've got 2 lines
            navbar.addClass('collapsed'); // force collapse mode
        }
    }

    $(document).on('ready', autocollapse);
    $(window).on('resize', autocollapse);
</script>

