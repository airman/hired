<div class="feed-info">
    <ul class="items-per-view">
        Results
        <li class="active">10</li>
        <li><a href="#">15</a></li>
        <li><a href="#">25</a></li>
        <li><a href="#">40</a></li>
    </ul>
    <ul class="pages">
        Pages
        <?php
            if($resultPage > 1){
                echo '<li><a href="#"><span class="glyphicon glyphicon-chevron-left"></a></span></li>' . PHP_EOL;
            }
            

            for($i=1; $i < $resultPageCount + 1; $i++){
                if($i == $resultPage)
                    echo "<li class=active>$i</li>\n";
                else
                    echo "<li>$i</li>\n";
            }

            if($resultPageCount > 1){
                echo '<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></a></span></li>' . PHP_EOL;
            }
        ?>
    </ul>
</div>
