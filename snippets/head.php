<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title><?php echo $view->getAttribute('title');?></title>
	<link rel="stylesheet" href="<?php echo HOME_URL;?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo HOME_URL;?>assets/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="<?php echo HOME_URL;?>assets/css/font-awesome.min.css"/>
    <script src="<?php echo HOME_URL;?>assets/js/jquery.js"></script>
	<script src="<?php echo HOME_URL;?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo HOME_URL;?>assets/js/alert.js"></script>
    <?php 
        $view->includeCSS();
        $view->includeJS();
    ?>
</head>
