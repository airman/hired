<?php
    define('ROOT', dirname(__FILE__) . '/');
    define('HOME_URL', 'http://localhost/hired/');
    define('HELPER_DIR', ROOT . 'helpers/');
    define('MODEL_DIR', ROOT . 'models/');
    define('VIEW_DIR', ROOT . 'views/');
    define('CLASS_DIR', ROOT . 'classes/');
    define('SNIPPET_DIR', ROOT . 'snippets/');
    define('ASSETS', HOME_URL . 'assets/');
    
    define('DSN', 'mysql');
    define('DB_HOST', 'localhost');
    define('DB_NAME', 'hired_db');
    define('DB_USER', 'root');
    define('DB_PASS', 'root');

